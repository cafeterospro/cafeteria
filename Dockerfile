//new Docker file 28/06
FROM openjdk:8-jdk-alpine
MAINTAINER covault08@gmail.com
EXPOSE 8069
RUN mkdir -p /opt/data/United-Commerce/
RUN chmod 755 /opt/data/United-Commerce/
ADD Cafeteria-0.0.1-SNAPSHOT.jar Cafeteria-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","Cafeteria-0.0.1-SNAPSHOT.jar"]

//OLD DOCKER FILE
FROM openjdk:8-jdk-alpine
EXPOSE 8096
ADD build/libs/Cafeteria-0.0.1-SNAPSHOT.jar Cafeteria-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","Cafeteria-0.0.1-SNAPSHOT.jar"]


FROM openjdk:8-jdk-alpine
MAINTAINER daniel@gmail.com
EXPOSE 8069
RUN mkdir -p /opt/data/United-Commerce/
RUN chmod 755 /opt/data/United-Commerce/
ADD Cafeteria-0.0.1-SNAPSHOT.jar Cafeteria-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","Cafeteria-0.0.1-SNAPSHOT.jar"]



