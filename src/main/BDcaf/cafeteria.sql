--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.10
-- Dumped by pg_dump version 9.6.10

-- Started on 2019-06-27 22:29:20

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2321 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 500 (class 1247 OID 16394)
-- Name: estado_domain; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN public.estado_domain AS character varying(8)
	CONSTRAINT estado_domain_check CHECK ((((VALUE)::text = 'activo'::text) OR ((VALUE)::text = 'inactivo'::text)));


ALTER DOMAIN public.estado_domain OWNER TO postgres;

--
-- TOC entry 502 (class 1247 OID 16396)
-- Name: estado_pago_domain; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN public.estado_pago_domain AS character varying(8)
	CONSTRAINT estado_pago_domain_check CHECK ((((VALUE)::text = 'pagada'::text) OR ((VALUE)::text = 'porpagar'::text)));


ALTER DOMAIN public.estado_pago_domain OWNER TO postgres;

--
-- TOC entry 587 (class 1247 OID 16412)
-- Name: id; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN public.id AS integer
	CONSTRAINT id_check CHECK (((VALUE >= 1) AND (VALUE <= '9999999999'::bigint)));


ALTER DOMAIN public.id OWNER TO postgres;

--
-- TOC entry 597 (class 1247 OID 16422)
-- Name: nivel_domain; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN public.nivel_domain AS character varying(8)
	CONSTRAINT nivel_domain_check CHECK ((((VALUE)::text = '0'::text) OR ((VALUE)::text = '1'::text) OR ((VALUE)::text = '2'::text)));


ALTER DOMAIN public.nivel_domain OWNER TO postgres;

--
-- TOC entry 593 (class 1247 OID 16418)
-- Name: positivo; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN public.positivo AS integer
	CONSTRAINT positivo_check CHECK (((VALUE >= 1) AND (VALUE <= '9999999999'::bigint)));


ALTER DOMAIN public.positivo OWNER TO postgres;

--
-- TOC entry 589 (class 1247 OID 16414)
-- Name: precio_domain; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN public.precio_domain AS numeric(12,2)
	CONSTRAINT precio_domain_check CHECK (((VALUE > (0)::numeric) AND (VALUE <= 9999999999.99)));


ALTER DOMAIN public.precio_domain OWNER TO postgres;

--
-- TOC entry 591 (class 1247 OID 16416)
-- Name: precio_domain_2; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN public.precio_domain_2 AS numeric(12,2)
	CONSTRAINT precio_domain_2_check CHECK (((VALUE >= (0)::numeric) AND (VALUE <= 9999999999.99)));


ALTER DOMAIN public.precio_domain_2 OWNER TO postgres;

--
-- TOC entry 595 (class 1247 OID 16420)
-- Name: t_pago; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN public.t_pago AS character varying(8)
	CONSTRAINT t_pago_check CHECK ((((VALUE)::text = 'efectivo'::text) OR ((VALUE)::text = 'debito'::text) OR ((VALUE)::text = 'credito'::text)));


ALTER DOMAIN public.t_pago OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 191 (class 1259 OID 16455)
-- Name: categoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categoria (
    id_categoria integer NOT NULL,
    nomb_categoria character varying(50) NOT NULL,
    estado public.estado_domain DEFAULT 'activo'::character varying NOT NULL
);


ALTER TABLE public.categoria OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 16453)
-- Name: categoria_id_categoria_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.categoria_id_categoria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categoria_id_categoria_seq OWNER TO postgres;

--
-- TOC entry 2322 (class 0 OID 0)
-- Dependencies: 190
-- Name: categoria_id_categoria_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.categoria_id_categoria_seq OWNED BY public.categoria.id_categoria;


--
-- TOC entry 189 (class 1259 OID 16437)
-- Name: cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cliente (
    dni public.id NOT NULL,
    nombre_cli character varying(30),
    direccion character varying(100),
    telefono character varying(15),
    estado public.estado_domain DEFAULT 'activo'::character varying NOT NULL
);


ALTER TABLE public.cliente OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16930)
-- Name: detalle_compra; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.detalle_compra (
    nro_factura public.positivo NOT NULL,
    ruc_proveedor public.id NOT NULL,
    id_producto character varying(10) NOT NULL,
    marca character varying(50) NOT NULL,
    cantidad integer NOT NULL,
    costo public.precio_domain NOT NULL
);


ALTER TABLE public.detalle_compra OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 16948)
-- Name: detalle_compra_tmp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.detalle_compra_tmp (
    nro_factura public.positivo NOT NULL,
    ruc_proveedor public.id NOT NULL,
    id_producto character varying(10) NOT NULL,
    marca character varying(50) NOT NULL,
    cantidad integer NOT NULL,
    costo public.precio_domain NOT NULL,
    nomb_categoria character varying(50),
    descripcion character varying(100),
    ubicacion character varying(150)
);


ALTER TABLE public.detalle_compra_tmp OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16894)
-- Name: detalle_factventa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.detalle_factventa (
    nrofac integer NOT NULL,
    id_producto character varying(10) NOT NULL,
    marca character varying(50) NOT NULL,
    cantidad smallint NOT NULL,
    pvp public.precio_domain NOT NULL
);


ALTER TABLE public.detalle_factventa OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16912)
-- Name: detalle_factventa_tempc; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.detalle_factventa_tempc (
    nrofac integer NOT NULL,
    id_producto character varying(10) NOT NULL,
    marca character varying(50) NOT NULL,
    cantidad smallint NOT NULL,
    pvp public.precio_domain NOT NULL
);


ALTER TABLE public.detalle_factventa_tempc OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 16576)
-- Name: devolucion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.devolucion (
    nro_dev integer NOT NULL,
    nrofac_d integer NOT NULL,
    motivo character varying(100) NOT NULL,
    fecha_d date,
    id_vendedor public.id NOT NULL
);


ALTER TABLE public.devolucion OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 16574)
-- Name: devolucion_nro_dev_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.devolucion_nro_dev_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.devolucion_nro_dev_seq OWNER TO postgres;

--
-- TOC entry 2323 (class 0 OID 0)
-- Dependencies: 196
-- Name: devolucion_nro_dev_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.devolucion_nro_dev_seq OWNED BY public.devolucion.nro_dev;


--
-- TOC entry 192 (class 1259 OID 16477)
-- Name: fact_compra; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fact_compra (
    nrofac public.positivo NOT NULL,
    f_ruc public.id NOT NULL,
    monto_total public.precio_domain NOT NULL,
    fecha date NOT NULL,
    estado public.estado_domain DEFAULT 'activo'::character varying NOT NULL,
    id_vendedor public.id
);


ALTER TABLE public.fact_compra OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 16527)
-- Name: fact_venta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fact_venta (
    nrofac integer NOT NULL,
    idcli public.id NOT NULL,
    monto_total public.precio_domain,
    fecha timestamp without time zone NOT NULL,
    tipo_pago public.t_pago NOT NULL,
    estado public.estado_domain DEFAULT 'activo'::character varying NOT NULL
);


ALTER TABLE public.fact_venta OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 16525)
-- Name: fact_venta_nrofac_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.fact_venta_nrofac_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fact_venta_nrofac_seq OWNER TO postgres;

--
-- TOC entry 2324 (class 0 OID 0)
-- Dependencies: 193
-- Name: fact_venta_nrofac_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.fact_venta_nrofac_seq OWNED BY public.fact_venta.nrofac;


--
-- TOC entry 202 (class 1259 OID 16776)
-- Name: item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.item (
    item_id integer NOT NULL,
    item_nombre character varying(50) NOT NULL,
    item_marca character varying(50) NOT NULL,
    item_categoria character varying(50) NOT NULL,
    item_descripcion character varying(100),
    item_existencia public.positivo NOT NULL,
    item_costo public.precio_domain NOT NULL,
    item_pvp public.precio_domain,
    item_estado public.estado_domain DEFAULT 'activo'::character varying NOT NULL,
    item_ubicacion character varying(150) DEFAULT ''::character varying,
    CONSTRAINT item_check CHECK (((item_pvp)::numeric <= ((item_costo)::numeric + ((item_costo)::numeric * 0.30))))
);


ALTER TABLE public.item OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16774)
-- Name: item_item_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.item_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.item_item_id_seq OWNER TO postgres;

--
-- TOC entry 2325 (class 0 OID 0)
-- Dependencies: 201
-- Name: item_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.item_item_id_seq OWNED BY public.item.item_id;


--
-- TOC entry 186 (class 1259 OID 16400)
-- Name: marca; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.marca (
    id_marca integer NOT NULL,
    nomb_marca character varying(50) NOT NULL,
    estado public.estado_domain DEFAULT 'activo'::character varying NOT NULL
);


ALTER TABLE public.marca OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 16398)
-- Name: marca_id_marca_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.marca_id_marca_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.marca_id_marca_seq OWNER TO postgres;

--
-- TOC entry 2326 (class 0 OID 0)
-- Dependencies: 185
-- Name: marca_id_marca_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.marca_id_marca_seq OWNED BY public.marca.id_marca;


--
-- TOC entry 203 (class 1259 OID 16876)
-- Name: producto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.producto (
    referencia character varying(10) NOT NULL,
    producto_nombre character varying(50) NOT NULL,
    nomb_marca character varying(50) NOT NULL,
    nomb_categoria character varying(50) NOT NULL,
    descripcion character varying(100),
    existencia public.positivo NOT NULL,
    costo public.precio_domain NOT NULL,
    pvp public.precio_domain,
    estado public.estado_domain DEFAULT 'activo'::character varying NOT NULL,
    ubicacion character varying(150) DEFAULT ''::character varying,
    CONSTRAINT producto_check CHECK (((pvp)::numeric <= ((costo)::numeric + ((costo)::numeric * 0.30))))
);


ALTER TABLE public.producto OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 16432)
-- Name: proveedor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.proveedor (
    ruc public.id NOT NULL,
    nombre_empresa character varying(30),
    correo character varying(30),
    direc_empresa character varying(100),
    nombre_responsable character varying(30),
    telef_responsable character varying(15)
);


ALTER TABLE public.proveedor OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 16560)
-- Name: tipo_pago; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_pago (
    nrofac_p integer NOT NULL,
    fecha_e date NOT NULL,
    fecha_p date,
    efectivo public.precio_domain_2,
    debito public.precio_domain_2,
    credito public.precio_domain_2,
    credito_especial public.precio_domain_2,
    estado public.estado_pago_domain DEFAULT 'porpagar'::character varying
);


ALTER TABLE public.tipo_pago OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 16609)
-- Name: tmp_compra; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tmp_compra (
    id_tmp integer NOT NULL,
    id_producto_tmp character varying(10) NOT NULL,
    marca_tmp character varying(50) NOT NULL,
    cantidad_tmp smallint NOT NULL,
    costo public.precio_domain NOT NULL,
    nomb_categoria character varying(50),
    descripcion character varying(100),
    ubicacion character varying(150)
);


ALTER TABLE public.tmp_compra OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16607)
-- Name: tmp_compra_cantidad_tmp_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tmp_compra_cantidad_tmp_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tmp_compra_cantidad_tmp_seq OWNER TO postgres;

--
-- TOC entry 2327 (class 0 OID 0)
-- Dependencies: 199
-- Name: tmp_compra_cantidad_tmp_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tmp_compra_cantidad_tmp_seq OWNED BY public.tmp_compra.cantidad_tmp;


--
-- TOC entry 198 (class 1259 OID 16605)
-- Name: tmp_compra_id_tmp_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tmp_compra_id_tmp_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tmp_compra_id_tmp_seq OWNER TO postgres;

--
-- TOC entry 2328 (class 0 OID 0)
-- Dependencies: 198
-- Name: tmp_compra_id_tmp_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tmp_compra_id_tmp_seq OWNED BY public.tmp_compra.id_tmp;


--
-- TOC entry 187 (class 1259 OID 16424)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario (
    nrodni public.id NOT NULL,
    clave character varying(5) NOT NULL,
    nivel public.nivel_domain NOT NULL,
    nombre character varying(15) NOT NULL,
    apellido character varying(15) NOT NULL,
    telefono character varying(15) NOT NULL,
    direccion character varying(100) NOT NULL
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- TOC entry 2106 (class 2604 OID 16458)
-- Name: categoria id_categoria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categoria ALTER COLUMN id_categoria SET DEFAULT nextval('public.categoria_id_categoria_seq'::regclass);


--
-- TOC entry 2112 (class 2604 OID 16579)
-- Name: devolucion nro_dev; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.devolucion ALTER COLUMN nro_dev SET DEFAULT nextval('public.devolucion_nro_dev_seq'::regclass);


--
-- TOC entry 2109 (class 2604 OID 16530)
-- Name: fact_venta nrofac; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fact_venta ALTER COLUMN nrofac SET DEFAULT nextval('public.fact_venta_nrofac_seq'::regclass);


--
-- TOC entry 2115 (class 2604 OID 16779)
-- Name: item item_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.item ALTER COLUMN item_id SET DEFAULT nextval('public.item_item_id_seq'::regclass);


--
-- TOC entry 2103 (class 2604 OID 16403)
-- Name: marca id_marca; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.marca ALTER COLUMN id_marca SET DEFAULT nextval('public.marca_id_marca_seq'::regclass);


--
-- TOC entry 2113 (class 2604 OID 16612)
-- Name: tmp_compra id_tmp; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tmp_compra ALTER COLUMN id_tmp SET DEFAULT nextval('public.tmp_compra_id_tmp_seq'::regclass);


--
-- TOC entry 2114 (class 2604 OID 16613)
-- Name: tmp_compra cantidad_tmp; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tmp_compra ALTER COLUMN cantidad_tmp SET DEFAULT nextval('public.tmp_compra_cantidad_tmp_seq'::regclass);


--
-- TOC entry 2297 (class 0 OID 16455)
-- Dependencies: 191
-- Data for Name: categoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.categoria (id_categoria, nomb_categoria, estado) FROM stdin;
2	cafes	activo
3	Postres	activo
4	Sanduches	activo
5	Bebidas	activo
\.


--
-- TOC entry 2329 (class 0 OID 0)
-- Dependencies: 190
-- Name: categoria_id_categoria_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categoria_id_categoria_seq', 5, true);


--
-- TOC entry 2295 (class 0 OID 16437)
-- Dependencies: 189
-- Data for Name: cliente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cliente (dni, nombre_cli, direccion, telefono, estado) FROM stdin;
\.


--
-- TOC entry 2312 (class 0 OID 16930)
-- Dependencies: 206
-- Data for Name: detalle_compra; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.detalle_compra (nro_factura, ruc_proveedor, id_producto, marca, cantidad, costo) FROM stdin;
\.


--
-- TOC entry 2313 (class 0 OID 16948)
-- Dependencies: 207
-- Data for Name: detalle_compra_tmp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.detalle_compra_tmp (nro_factura, ruc_proveedor, id_producto, marca, cantidad, costo, nomb_categoria, descripcion, ubicacion) FROM stdin;
\.


--
-- TOC entry 2310 (class 0 OID 16894)
-- Dependencies: 204
-- Data for Name: detalle_factventa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.detalle_factventa (nrofac, id_producto, marca, cantidad, pvp) FROM stdin;
\.


--
-- TOC entry 2311 (class 0 OID 16912)
-- Dependencies: 205
-- Data for Name: detalle_factventa_tempc; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.detalle_factventa_tempc (nrofac, id_producto, marca, cantidad, pvp) FROM stdin;
\.


--
-- TOC entry 2303 (class 0 OID 16576)
-- Dependencies: 197
-- Data for Name: devolucion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.devolucion (nro_dev, nrofac_d, motivo, fecha_d, id_vendedor) FROM stdin;
\.


--
-- TOC entry 2330 (class 0 OID 0)
-- Dependencies: 196
-- Name: devolucion_nro_dev_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.devolucion_nro_dev_seq', 1, false);


--
-- TOC entry 2298 (class 0 OID 16477)
-- Dependencies: 192
-- Data for Name: fact_compra; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fact_compra (nrofac, f_ruc, monto_total, fecha, estado, id_vendedor) FROM stdin;
\.


--
-- TOC entry 2300 (class 0 OID 16527)
-- Dependencies: 194
-- Data for Name: fact_venta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fact_venta (nrofac, idcli, monto_total, fecha, tipo_pago, estado) FROM stdin;
\.


--
-- TOC entry 2331 (class 0 OID 0)
-- Dependencies: 193
-- Name: fact_venta_nrofac_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fact_venta_nrofac_seq', 1, true);


--
-- TOC entry 2308 (class 0 OID 16776)
-- Dependencies: 202
-- Data for Name: item; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.item (item_id, item_nombre, item_marca, item_categoria, item_descripcion, item_existencia, item_costo, item_pvp, item_estado, item_ubicacion) FROM stdin;
1	PIES	MCDONNALDS	Postres	DULCE DE MANZANA	3	10.00	13.00	activo	
\.


--
-- TOC entry 2332 (class 0 OID 0)
-- Dependencies: 201
-- Name: item_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.item_item_id_seq', 1, true);


--
-- TOC entry 2292 (class 0 OID 16400)
-- Dependencies: 186
-- Data for Name: marca; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.marca (id_marca, nomb_marca, estado) FROM stdin;
9	amazonas	activo
10	madrid	activo
11	uhjajs	activo
12	Negra	activo
\.


--
-- TOC entry 2333 (class 0 OID 0)
-- Dependencies: 185
-- Name: marca_id_marca_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.marca_id_marca_seq', 12, true);


--
-- TOC entry 2309 (class 0 OID 16876)
-- Dependencies: 203
-- Data for Name: producto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.producto (referencia, producto_nombre, nomb_marca, nomb_categoria, descripcion, existencia, costo, pvp, estado, ubicacion) FROM stdin;
1234	PIES	MCDONNALDS	Postres	DULCE DE MANZANA	3	10.00	13.00	activo	primer estante
1119	TORTA	NEVADA	Postres	TORTA MANZANA	3	10.00	13.00	activo	ubicacion
1110	Cafe Regular	America	Bebidas	Cafe regular	3	10.00	13.00	activo	ubicacion
1112	Cafe Decafeinado	America	Bebidas	Cafe Decafeinado	3	10.00	13.00	activo	ubicacion
\.


--
-- TOC entry 2294 (class 0 OID 16432)
-- Dependencies: 188
-- Data for Name: proveedor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proveedor (ruc, nombre_empresa, correo, direc_empresa, nombre_responsable, telef_responsable) FROM stdin;
123	chevi	chevi@gmail.com	el valle	Daniel	414-123-4789
124	mayorca	cheviplanet@gmail.com	el valle	Danielo	414-123-9874
\.


--
-- TOC entry 2301 (class 0 OID 16560)
-- Dependencies: 195
-- Data for Name: tipo_pago; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_pago (nrofac_p, fecha_e, fecha_p, efectivo, debito, credito, credito_especial, estado) FROM stdin;
\.


--
-- TOC entry 2306 (class 0 OID 16609)
-- Dependencies: 200
-- Data for Name: tmp_compra; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tmp_compra (id_tmp, id_producto_tmp, marca_tmp, cantidad_tmp, costo, nomb_categoria, descripcion, ubicacion) FROM stdin;
\.


--
-- TOC entry 2334 (class 0 OID 0)
-- Dependencies: 199
-- Name: tmp_compra_cantidad_tmp_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tmp_compra_cantidad_tmp_seq', 1, false);


--
-- TOC entry 2335 (class 0 OID 0)
-- Dependencies: 198
-- Name: tmp_compra_id_tmp_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tmp_compra_id_tmp_seq', 1, false);


--
-- TOC entry 2293 (class 0 OID 16424)
-- Dependencies: 187
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuario (nrodni, clave, nivel, nombre, apellido, telefono, direccion) FROM stdin;
100	100	0	Daniel	Covault	-3574840	El valle
200	200	1	David	Covault	-3554840	El valle
\.


--
-- TOC entry 2133 (class 2606 OID 16466)
-- Name: categoria categoria_nomb_categoria_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categoria
    ADD CONSTRAINT categoria_nomb_categoria_key UNIQUE (nomb_categoria);


--
-- TOC entry 2123 (class 2606 OID 16411)
-- Name: marca marca_nomb_marca_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.marca
    ADD CONSTRAINT marca_nomb_marca_key UNIQUE (nomb_marca);


--
-- TOC entry 2131 (class 2606 OID 16441)
-- Name: cliente pk_cliente; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT pk_cliente PRIMARY KEY (dni);


--
-- TOC entry 2153 (class 2606 OID 16901)
-- Name: detalle_factventa pk_detal_factv; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_factventa
    ADD CONSTRAINT pk_detal_factv PRIMARY KEY (nrofac, id_producto);


--
-- TOC entry 2155 (class 2606 OID 16919)
-- Name: detalle_factventa_tempc pk_detal_factv_temp; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_factventa_tempc
    ADD CONSTRAINT pk_detal_factv_temp PRIMARY KEY (nrofac, id_producto);


--
-- TOC entry 2145 (class 2606 OID 16618)
-- Name: tmp_compra pk_detal_prod_tmp; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tmp_compra
    ADD CONSTRAINT pk_detal_prod_tmp PRIMARY KEY (id_tmp);


--
-- TOC entry 2143 (class 2606 OID 16581)
-- Name: devolucion pk_dev_fact; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.devolucion
    ADD CONSTRAINT pk_dev_fact PRIMARY KEY (nro_dev, nrofac_d);


--
-- TOC entry 2127 (class 2606 OID 16431)
-- Name: usuario pk_empleado; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT pk_empleado PRIMARY KEY (nrodni);


--
-- TOC entry 2137 (class 2606 OID 16485)
-- Name: fact_compra pk_factcomp; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fact_compra
    ADD CONSTRAINT pk_factcomp PRIMARY KEY (nrofac, f_ruc);


--
-- TOC entry 2139 (class 2606 OID 16536)
-- Name: fact_venta pk_factvent; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fact_venta
    ADD CONSTRAINT pk_factvent PRIMARY KEY (nrofac);


--
-- TOC entry 2147 (class 2606 OID 16787)
-- Name: item pk_item; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.item
    ADD CONSTRAINT pk_item PRIMARY KEY (item_id, item_nombre, item_categoria);


--
-- TOC entry 2125 (class 2606 OID 16409)
-- Name: marca pk_marca; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.marca
    ADD CONSTRAINT pk_marca PRIMARY KEY (id_marca);


--
-- TOC entry 2135 (class 2606 OID 16464)
-- Name: categoria pk_model; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categoria
    ADD CONSTRAINT pk_model PRIMARY KEY (id_categoria);


--
-- TOC entry 2149 (class 2606 OID 16886)
-- Name: producto pk_producto; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.producto
    ADD CONSTRAINT pk_producto PRIMARY KEY (producto_nombre, nomb_categoria);


--
-- TOC entry 2129 (class 2606 OID 16436)
-- Name: proveedor pk_proveedor; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proveedor
    ADD CONSTRAINT pk_proveedor PRIMARY KEY (ruc);


--
-- TOC entry 2157 (class 2606 OID 16937)
-- Name: detalle_compra pk_tiene; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_compra
    ADD CONSTRAINT pk_tiene PRIMARY KEY (nro_factura, ruc_proveedor, id_producto);


--
-- TOC entry 2159 (class 2606 OID 16955)
-- Name: detalle_compra_tmp pk_tiene_tmp; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_compra_tmp
    ADD CONSTRAINT pk_tiene_tmp PRIMARY KEY (nro_factura, ruc_proveedor, id_producto);


--
-- TOC entry 2141 (class 2606 OID 16568)
-- Name: tipo_pago pk_tipo_pago; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_pago
    ADD CONSTRAINT pk_tipo_pago PRIMARY KEY (nrofac_p);


--
-- TOC entry 2151 (class 2606 OID 16888)
-- Name: producto producto_referencia_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.producto
    ADD CONSTRAINT producto_referencia_key UNIQUE (referencia);


--
-- TOC entry 2161 (class 2606 OID 16537)
-- Name: fact_venta fk_cliente; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fact_venta
    ADD CONSTRAINT fk_cliente FOREIGN KEY (idcli) REFERENCES public.cliente(dni) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2160 (class 2606 OID 16486)
-- Name: fact_compra fk_identif; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fact_compra
    ADD CONSTRAINT fk_identif FOREIGN KEY (f_ruc) REFERENCES public.proveedor(ruc) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2170 (class 2606 OID 16938)
-- Name: detalle_compra fk_identif; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_compra
    ADD CONSTRAINT fk_identif FOREIGN KEY (nro_factura, ruc_proveedor) REFERENCES public.fact_compra(nrofac, f_ruc) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2172 (class 2606 OID 16956)
-- Name: detalle_compra_tmp fk_identif_tmp; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_compra_tmp
    ADD CONSTRAINT fk_identif_tmp FOREIGN KEY (nro_factura, ruc_proveedor) REFERENCES public.fact_compra(nrofac, f_ruc) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2164 (class 2606 OID 16788)
-- Name: item fk_item_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.item
    ADD CONSTRAINT fk_item_categoria FOREIGN KEY (item_categoria) REFERENCES public.categoria(nomb_categoria) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2165 (class 2606 OID 16889)
-- Name: producto fk_item_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.producto
    ADD CONSTRAINT fk_item_categoria FOREIGN KEY (nomb_categoria) REFERENCES public.categoria(nomb_categoria) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2162 (class 2606 OID 16569)
-- Name: tipo_pago fk_nro_factv; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_pago
    ADD CONSTRAINT fk_nro_factv FOREIGN KEY (nrofac_p) REFERENCES public.fact_venta(nrofac) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2163 (class 2606 OID 16582)
-- Name: devolucion fk_nro_factv; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.devolucion
    ADD CONSTRAINT fk_nro_factv FOREIGN KEY (nrofac_d) REFERENCES public.fact_venta(nrofac) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2166 (class 2606 OID 16902)
-- Name: detalle_factventa fk_nro_factv; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_factventa
    ADD CONSTRAINT fk_nro_factv FOREIGN KEY (nrofac) REFERENCES public.fact_venta(nrofac) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2168 (class 2606 OID 16920)
-- Name: detalle_factventa_tempc fk_nro_factv; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_factventa_tempc
    ADD CONSTRAINT fk_nro_factv FOREIGN KEY (nrofac) REFERENCES public.fact_venta(nrofac) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2167 (class 2606 OID 16907)
-- Name: detalle_factventa fk_prod; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_factventa
    ADD CONSTRAINT fk_prod FOREIGN KEY (id_producto) REFERENCES public.producto(referencia) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2169 (class 2606 OID 16925)
-- Name: detalle_factventa_tempc fk_prod; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_factventa_tempc
    ADD CONSTRAINT fk_prod FOREIGN KEY (id_producto) REFERENCES public.producto(referencia) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2171 (class 2606 OID 16943)
-- Name: detalle_compra fk_prod; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_compra
    ADD CONSTRAINT fk_prod FOREIGN KEY (id_producto) REFERENCES public.producto(referencia) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2173 (class 2606 OID 16961)
-- Name: detalle_compra_tmp fk_prod_tmp; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.detalle_compra_tmp
    ADD CONSTRAINT fk_prod_tmp FOREIGN KEY (id_producto) REFERENCES public.producto(referencia) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2019-06-27 22:29:21

--
-- PostgreSQL database dump complete
--

	alter table fact_venta add column created_by id
	ALTER TABLE usuario ADD CONSTRAINT pk_usuario UNIQUE (nrodni);
	ALTER TABLE fact_venta add
CONSTRAINT fk_fact_venta FOREIGN KEY (created_by)
        REFERENCES public.usuario (nrodni) MATCH SIMPLE
        ON UPDATE CASCADE

alter table item add precio_costo public.precio_domain NOT NULL default 0.01

--Creación de la tabla unidades
CREATE TABLE unidad
(
  	unidad_id serial NOT NULL,
  	unidad_nombre varchar,
	unidad_estado public.estado_domain DEFAULT 'activo'::character varying NOT NULL,
  PRIMARY KEY (unidad_id));

--Creación de la tabla receta
  	 	CREATE TABLE receta
          (
            producto_padre int NOT NULL,
            producto_hijo int NOT NULL,
			hijo_cantidad_bruta real NOT NULL,
        	factor_conversion real,
          	unidad_medida int NOT NULL,
           	hijo_precio_compra public.precio_domain NOT NULL default 0.01,
          	hijo_costo public.precio_domain NOT NULL default 0.01,
          	receta_estado public.estado_domain DEFAULT 'activo'::character varying NOT NULL,
            PRIMARY KEY (producto_padre, producto_hijo),
          	CONSTRAINT fk_idpadre_receta FOREIGN KEY (producto_padre) REFERENCES public.item (iditem) MATCH SIMPLE ON UPDATE CASCADE,
          	CONSTRAINT fk_idhijo_receta FOREIGN KEY (producto_hijo) REFERENCES public.item (iditem) MATCH SIMPLE ON UPDATE CASCADE,
          	CONSTRAINT fk_unidad_receta FOREIGN KEY (unidad_medida) REFERENCES public.unidad (unidad_id) MATCH SIMPLE ON UPDATE CASCADE);

--Inseratar campos en la tabbla receta para los ingredientes de cafe

INSERT INTO public.receta(
	producto_padre, producto_hijo, factor_conversion, hijo_costo, hijo_precio_compra, hijo_cantidad_bruta, unidad_medida)
	VALUES (13, 10, 1, 0.34, 33, 0.01, 2);

INSERT INTO public.receta(
	producto_padre, producto_hijo, factor_conversion, hijo_costo, hijo_precio_compra, hijo_cantidad_bruta, unidad_medida)
	VALUES (13, 11, 20, 0.31, 25, 0.25, 4);

INSERT INTO public.receta(
	producto_padre, producto_hijo, factor_conversion, hijo_costo, hijo_precio_compra, hijo_cantidad_bruta, unidad_medida)
	VALUES (13, 12, 50, 0.04, 100, 0.02, 2);

  	        INSERT INTO categoria (nombcategoria) VALUES (INGREDIENTE);
            INSERT INTO categoria (nomb_categoria) VALUES ('BEBIDA');
            UPDATE categoria set nomb_categoria = 'INGREDIENTE' where id_categoria = 7;

        INSERT INTO public.item(
            nombre, nombcategoria, descripcion, costo, precio_costo)
            VALUES ('CAFE MOLIDO','INGREDIENTE', 'PAQUETE DE CAFE MOLIDO PARA PREPARAR CAFE', 33.60, 0.34);

                INSERT INTO public.item(
            nombre, nombcategoria, descripcion, costo, precio_costo)
            VALUES ('AGUA BONAFONT','INGREDIENTE', 'AGUA BONAFONT 20 LITRO', 25.00, 0.31);

                        INSERT INTO public.item(
            nombre, nombcategoria, descripcion, costo, precio_costo)
            VALUES ('AZUCAR','INGREDIENTE', 'AZUCAR SACO 50KG', 100, 0.04);

         INSERT INTO public.item(
            nombre, nombcategoria, descripcion, costo, precio_costo)
            VALUES ('CAFE','BEBIDAS', 'CAFE', 100, 0.04);

            INSERT INTO public.unidad(
    	unidad_nombre)
    	VALUES ('KILO');

            INSERT INTO public.unidad(
    	unidad_nombre)
    	VALUES ('LITRO');

        	DELETE FROM public.unidad
    	WHERE unidad_id = 3;

        INSERT INTO public.receta(
    	producto_padre, producto_hijo, producto_hijo_costo, producto_hijo_cantidad, unidad)
    	VALUES (13, 10 , ?, ?, 2);

select iditem, name , (select precio_costo from item where iditem = detalle_factventa.idItem) AS costo, pvp, SUM(cantidad) as cantidad, ((select precio_costo from item where iditem = detalle_factventa.idItem) * (SUM(cantidad))) AS costo_total, SUM(preciototal) as totalxproducto
from detalle_factventa where nrofac in
(select nrofac from fact_venta where fecha between (select date_trunc('month', current_date))
and current_timestamp)group by iditem, name, pvp;



-------------------------------------modificaciones para multiempresa bd qa--------------------------
--Creacion de la tabla Empresa:

    CREATE TABLE empresa (
    id_empresa serial NOT NULL,
    codigo character varying(20) NOT NULL,
    nomb_empresa character varying(50) NOT NULL,
    correo character varying(50) NOT NULL,
    telefono character varying(15) NOT NULL,
    localidad character varying(50) NOT NULL,
    direccion character varying(100) NOT NULL,
    PRIMARY KEY (id_empresa)
    );

    INSERT INTO public.empresa(codigo, nomb_empresa, correo, telefono, localidad, direccion)
    VALUES ('20605020578','HAMBURGUESA GOURMET.SAC', 'rjosejromero@gmail.com', '960497188', 'LIMA','JIRON RECABARREN 1270 SURQUILLO');

     INSERT INTO public.empresa(codigo, nomb_empresa, correo, telefono, localidad, direccion)
     VALUES ('20605020578','LA HUERTA.SAC', 'nicol.conde.duque@gmail.com', '981167097', 'LIMA','JESUS MARIA');

	alter table categoria add column id_empresa id;
	ALTER TABLE categoria add
    CONSTRAINT fk_id_empresa FOREIGN KEY (id_empresa)
    REFERENCES public.empresa (id_empresa) MATCH SIMPLE
    ON UPDATE CASCADE;


	alter table usuario add column id_empresa id;
	ALTER TABLE usuario add
    CONSTRAINT fk_id_empresa FOREIGN KEY (id_empresa)
    REFERENCES public.empresa (id_empresa) MATCH SIMPLE
    ON UPDATE CASCADE;

    --agregarle el campo estado a la tabla de usuarios

    alter table usuario add column estado estado_domain DEFAULT 'activo'::character varying NOT NULL;

    -- se le asignaron los empleados a la empresa 1
    UPDATE usuario SET id_empresa = 1

    --agregarle primary key a la tabla usuario

  	ALTER TABLE usuario
    ADD CONSTRAINT usuario_pk
    PRIMARY KEY (nrodni, id_empresa);

    --agregarle a los items el campo empresa
    alter table item add column id_empresa id;

    ALTER TABLE item add
    CONSTRAINT fk_id_empresa FOREIGN KEY (id_empresa)
    REFERENCES public.empresa (id_empresa) MATCH SIMPLE
    ON UPDATE CASCADE;

    --eliminar el contraint de la clave primaria de categoria
    --colocar el id categoria como unico
    ALTER TABLE categoria ADD UNIQUE (id_categoria);

    ALTER TABLE categoria
    DROP CONSTRAINT pk_model;

      UPDATE categoria SET id_empresa = 1

	ALTER TABLE categoria
    ADD CONSTRAINT categoria_pk
    PRIMARY KEY (id_categoria, id_empresa);

    -- acambios para la tabla de item
    --se debe agregar el id_categoria en vez del nombre_categoria

    --agregar el constraint del id categoria referenciada de la tabla categoria
    alter table item add column id_categoria id;
    ALTER TABLE item add
    CONSTRAINT fk_id_categoria FOREIGN KEY (id_categoria)
    REFERENCES public.categoria (id_categoria) MATCH SIMPLE
    ON UPDATE CASCADE;

    --agregar los id de los items
    UPDATE item SET id_categoria = (select id_categoria from categoria where nomb_categoria = 'DESAYUNOS')
    where nombcategoria = 'DESAYUNOS';

    UPDATE item SET id_categoria = (select id_categoria from categoria where nomb_categoria = 'ALMUERZOS')
    where nombcategoria = 'ALMUERZOS';

    UPDATE item SET id_categoria = (select id_categoria from categoria where nomb_categoria = 'HAMBURGUESAS')
    where nombcategoria = 'HAMBURGUESAS';

    UPDATE item SET id_categoria = (select id_categoria from categoria where nomb_categoria = 'PERRO CALIENTE')
    where nombcategoria = 'PERRO CALIENTE';

    UPDATE item SET id_categoria = (select id_categoria from categoria where nomb_categoria = 'PAPAS')
    where nombcategoria = 'PAPAS';

    UPDATE item SET id_categoria = (select id_categoria from categoria where nomb_categoria = 'EXTRAS')
    where nombcategoria = 'EXTRAS';

    UPDATE item SET id_categoria = (select id_categoria from categoria where nomb_categoria = 'BEBIDAS')
    where nombcategoria = 'BEBIDAS';

    -- colocarle a la empresa 1 como propietario
    UPDATE item SET id_empresa = 1 where estado = 'activo';

    --eliminar el constraint del pk_item
    ALTER TABLE item
    DROP CONSTRAINT pk_item;

    --creale la clave primaria compuesta del iditem, id_categoria_id_empresa
	ALTER TABLE item
    ADD CONSTRAINT item_pk
    PRIMARY KEY (iditem, id_categoria, id_empresa);

    -- cambios para factventa se le agrega la columna id_empresa
    --volver unico el numero de factura
    ALTER TABLE fact_venta ADD UNIQUE (nrofac);

    --agregar colimna id_empresa a la tabla fact_venta
	alter table fact_venta add column id_empresa id;
	ALTER TABLE fact_venta add
    CONSTRAINT fk_id_factventa FOREIGN KEY (id_empresa)
    REFERENCES public.empresa (id_empresa) MATCH SIMPLE
    ON UPDATE CASCADE;

    -- colocarle a la empresa 1 como propietario
    UPDATE fact_venta SET id_empresa = 1 where estado = 'activo';

    --asignarle las facturas sin creador al señor jose
    UPDATE fact_venta SET created_by = 5530109 where created_by is null;

     --clave primaria de factura de venta es el nuemro de factura el nrodni del vendedor y el id de la empresa
	 ALTER TABLE fact_venta
     ADD CONSTRAINT fact_venta_pk
     PRIMARY KEY (nrofac, created_by, id_empresa);


    --agregar una columna serial
    alter table detalle_factventa add column id_empresa id;

     --setear id_empresa con el valor 1
     UPDATE detalle_factventa SET id_empresa = 1 where id_empresa is null;

    --agregar una columna serial
    alter table detalle_factventa add column id_detalle_fact_venta serial;

	ALTER TABLE detalle_factventa ADD
    CONSTRAINT fk_nro_detalle_factventa FOREIGN KEY (id_empresa)
    REFERENCES public.fact_venta (nrofac) MATCH SIMPLE
    ON UPDATE CASCADE;

    --agregarle primary key a la tabla detalle_factventa
    ALTER TABLE detalle_factventa
    ADD CONSTRAINT detalle_fact_venta
    PRIMARY KEY (id_detalle_fact_venta, nrofac, id_empresa);

 ---------------- ADD ITEMS IMAGES-------------

   -- add image_paht column to items
   alter table item add column image_path VARCHAR;
   --asignarle a todas las imagenes existentes la imagen por defecto
   UPDATE item SET image_path = 'default-uc-grey.jpg' where estado = 'activo';


   --create table status_ordrers
   CREATE TABLE status_order (
      id_status_order serial unique,
      name_status_order character varying(50) NOT NULL,
      PRIMARY KEY (id_status_order)
   );

   --insert value into status orders
INSERT INTO public.status_order(name_status_order) VALUES ('ESPERA');

INSERT INTO public.status_order(name_status_order) VALUES ('TERMINADO');

INSERT INTO public.status_order(name_status_order) VALUES ('COMPLETADO');

   --add column status_orders into fact_venta
    alter table fact_venta add column id_status_order integer default 1;
   ALTER TABLE fact_venta
       ADD CONSTRAINT fk_id_status_order FOREIGN KEY (id_status_order)
       REFERENCES public.status_order (id_status_order) MATCH SIMPLE
       ON UPDATE CASCADE;
       
UPDATE fact_venta SET id_status_order = '3' where id_status_order = '1';

alter table usuario add column email character varying(30) UNIQUE;

select
    c.estado,
    c.id_categoria,
    c.id_empresa,
    c.nomb_categoria
from
    public.categoria c;

select iditem, name , (select precio_costo from public.item where iditem = public.detalle_factventa.idItem) AS costo, pvp, SUM(cantidad) as cantidad,
((select precio_costo from public.item where iditem = public.detalle_factventa.idItem) * (SUM(cantidad))) AS costo_total, SUM(preciototal) as totalxproducto
            from public.detalle_factventa where nrofac in
            (select nrofac from public.fact_venta where fecha between '2021-05-01'
            and '2021-05-31') and id_empresa = 1 group by iditem, name, pvp;

select * from public.usuario;
