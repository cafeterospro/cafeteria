package com.cafeteria.Cafeteria.serviceImpl;

import com.cafeteria.Cafeteria.dao.UserMapper;
import com.cafeteria.Cafeteria.domain.BaseResponse;
import com.cafeteria.Cafeteria.domain.User;
import com.cafeteria.Cafeteria.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service("UserService")

public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;

    private final static Logger LOGGER = Logger.getLogger("bitacora.subnivel.Control");

    @Override
    public BaseResponse<User> loginUser(User user) {
        BaseResponse<User> response = new BaseResponse<>();
        try {
            User responseUser = userMapper.loginUser(user);
            if (responseUser!=null){
                response.setData(responseUser);
                response.setStatus(true);
                response.setMessage("Credenciales correctas.");
            }else{
                response.setStatus(false);
                response.setMessage("Credenciales incorrectas.");
            }
        }catch (Exception ex){
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return  response;
    }

    @Override
    public BaseResponse<User> login(User user) {
        BaseResponse<User> response = new BaseResponse<>();
        try {
            User responseUser = userMapper.login(user);
            if (responseUser!=null){
                response.setData(responseUser);
                response.setStatus(true);
                response.setMessage("Credenciales correctas.");
            }else{
                response.setStatus(false);
                response.setMessage("Credenciales incorrectas.");
            }
        }catch (Exception ex){
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return  response;
    }

    @Override
    public BaseResponse<List<User>> searchUser(int nrodni) {
        BaseResponse<List<User>> response = new BaseResponse<>();
        try {
            ArrayList<User> responseItem = userMapper.searchUser(nrodni);
            if (responseItem != null){
                response.setData(responseItem);
                response.setStatus(true);
                response.setMessage("Se desactivo el usuario");
            }else{
                response.setStatus(false);
                response.setMessage("No se logro desacticvar el usuario");
            }
        }catch (Exception ex){
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return  response;
    }

    @Override
    public BaseResponse<List<User>> getListUsers(int id_empresa) {
        BaseResponse<List<User>> response = new BaseResponse<>();
        try {
            List<User> responseUser = userMapper.getListUsers(id_empresa);
            if (responseUser!=null){
                response.setData(responseUser);
                response.setStatus(true);
                response.setMessage("Resultado de la lista de usuarios");
            }else{
                response.setStatus(false);
                response.setMessage("La lista esta vacia");
            }
        }catch (Exception ex){
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return  response;
    }

    @Override
    public BaseResponse<Integer> newUser(User user) {
        BaseResponse<Integer> response = new BaseResponse<>();
        try {
            int responseItem = userMapper.newUser(user);
            if (responseItem != 0){
                response.setData(responseItem);
                response.setStatus(true);
                response.setMessage("Se argego el usuario correctamente");
            }else{
                response.setStatus(false);
                response.setMessage("No se logro agregar el usuario");
            }
        }catch (Exception ex){
            response.setStatus(false);
            response.setMessage("No se logro agregar el usuario");
            LOGGER.log(Level.INFO, ex.getMessage());
            //response.setMessage(ex.getMessage());
        }
        return  response;
    }

    @Override
    public BaseResponse<User> createUserAccount(User user) {
        BaseResponse<User> response = new BaseResponse<>();
        try {
            int responseItem = userMapper.createUserAccount(user);
            if (responseItem != 0){
                response = login(user);
            }else{
                response.setStatus(false);
                response.setMessage("No se logro agregar el usuario");
            }
        }catch (Exception ex){
            response.setStatus(false);
            response.setMessage("No se logro agregar el usuario");
            LOGGER.log(Level.INFO, ex.getMessage());
        }
        return  response;
    }

    @Override
    public BaseResponse<Integer> updateUser(User user) {
        BaseResponse<Integer> response = new BaseResponse<>();
        try {
            int responseItem = userMapper.updateUser(user);
            if (responseItem != 0){
                response.setData(responseItem);
                response.setStatus(true);
                response.setMessage("Se modifico el usuario correctamente");
            }else{
                response.setStatus(false);
                response.setMessage("No se logro modificar el usuario");
            }
        }catch (Exception ex){
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return  response;
    }

    @Override
    public BaseResponse<Integer> deleteUser(Integer nrodni) {
        BaseResponse<Integer> response = new BaseResponse<>();
        try {
            int responseItem = userMapper.deleteUser(nrodni);
            if (responseItem != 0){
                response.setData(responseItem);
                response.setStatus(true);
                response.setMessage("Se desactivo el usuario");
            }else{
                response.setStatus(false);
                response.setMessage("No se logro desacticvar el usuario");
            }
        }catch (Exception ex){
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return  response;
    }

}
