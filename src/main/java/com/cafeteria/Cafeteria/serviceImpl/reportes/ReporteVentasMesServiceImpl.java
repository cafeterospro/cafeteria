package com.cafeteria.Cafeteria.serviceImpl.reportes;

import com.cafeteria.Cafeteria.dao.reportes.ReporteVentasMesMapper;
import com.cafeteria.Cafeteria.domain.BaseResponse;
import com.cafeteria.Cafeteria.domain.reportes.ReporteVentasResponseEntity;
import com.cafeteria.Cafeteria.service.reportes.ReporteVentasMesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("reporteVentasMesService")
public class ReporteVentasMesServiceImpl implements ReporteVentasMesService{

    @Autowired
    ReporteVentasMesMapper reporteVentasMesMapper;
        @Override
        public BaseResponse<List<ReporteVentasResponseEntity>> obtenerListReporteVentasActual(int id_emrpres) {
            BaseResponse<List<ReporteVentasResponseEntity>> response = new BaseResponse<>();
            try {
                List<ReporteVentasResponseEntity> responseItem = reporteVentasMesMapper.obtenerListReporteVentasActual(id_emrpres);
                if (responseItem!=null){
                    response.setData(responseItem);
                    response.setStatus(true);
                    response.setMessage("Resultado de la lista de Items");
                }else{
                    response.setStatus(false);
                    response.setMessage("La Lista esta vacia");
                }
            }catch (Exception ex){
                response.setStatus(false);
                response.setMessage(ex.getMessage());
            }
            return  response;
        }

    @Override
    public BaseResponse<List<ReporteVentasResponseEntity>> obtenerListReporteVentas(String fechaIni, String fechaFin) {
        BaseResponse<List<ReporteVentasResponseEntity>> response = new BaseResponse<>();
        try {
            List<ReporteVentasResponseEntity> responseItem = reporteVentasMesMapper.obtenerListReporteVentas(fechaIni, fechaFin);
            if (responseItem!=null){
                response.setData(responseItem);
                response.setStatus(true);
                response.setMessage("Resultado de la lista de Items");
            }else{
                response.setStatus(false);
                response.setMessage("La Lista esta vacia");
            }
        }catch (Exception ex){
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return  response;
    }

    public BaseResponse<List<ReporteVentasResponseEntity>> obtenerListReporteVentasMesActual(int id_emrpres) {
        BaseResponse<List<ReporteVentasResponseEntity>> response = new BaseResponse<>();
        try {
            List<ReporteVentasResponseEntity> responseItem = reporteVentasMesMapper. obtenerListReporteVentasMesActual(id_emrpres);
            if (responseItem!=null){
                response.setData(responseItem);
                response.setStatus(true);
                response.setMessage("Resultado de la lista de Items");
            }else{
                response.setStatus(false);
                response.setMessage("La Lista esta vacia");
            }
        }catch (Exception ex){
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return  response;
    }


}

