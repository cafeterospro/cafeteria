package com.cafeteria.Cafeteria.serviceImpl;

import com.cafeteria.Cafeteria.dao.DetalleOrdenMapper;
import com.cafeteria.Cafeteria.domain.DetalleOrden;
import com.cafeteria.Cafeteria.service.DetalleOrdenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("detalleOrdenService")
public class DetalleOrdenImpl implements DetalleOrdenService {

    @Autowired
    DetalleOrdenMapper detalleOrdenMapper;

    @Override
    public List<DetalleOrden> getListDetalleOrden() {
        return detalleOrdenMapper.getListDetalleOrden();
    }

    @Override
    public List<DetalleOrden> getDetalleOrden(int nrofac) {
        return detalleOrdenMapper.getDetalleOrden(nrofac);
    }
}
