package com.cafeteria.Cafeteria.serviceImpl;

import com.cafeteria.Cafeteria.dao.ItemMapper;
import com.cafeteria.Cafeteria.domain.BaseResponse;
import com.cafeteria.Cafeteria.domain.ItemEntity;
import com.cafeteria.Cafeteria.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service("itemService")
public class ItemServiceImpl implements ItemService {
    private String UPLOAD_FOLDER = "/opt/data/United-Commerce/";
    //private String UPLOAD_FOLDER = "//Users//everis//Documents//assets-united-commerce//img//items//";

    @Autowired
    ItemMapper itemMapper;

    @Override
    public BaseResponse<List<ItemEntity>> obtenerListItems(Integer id_empresa) {
            BaseResponse<List<ItemEntity>> response = new BaseResponse<>();
            try {
                List<ItemEntity> responseItem = itemMapper.obtenerListItems(id_empresa);
                if (responseItem!=null){
                    response.setData(responseItem);
                    response.setStatus(true);
                    response.setMessage("Resultado de la lista de Items");
                }else{
                    response.setStatus(false);
                    response.setMessage("La Lista esta vacia");
                }
            }catch (Exception ex){
                response.setStatus(false);
                response.setMessage(ex.getMessage());
            }
            return  response;
        }

    @Override
    public BaseResponse<List<ItemEntity>> getListItemsWeb(Integer id_empresa) {
        BaseResponse<List<ItemEntity>> response = new BaseResponse<>();
        try {
            List<ItemEntity> responseItem = itemMapper.getListItemsWeb(id_empresa);
            if (responseItem!=null){
                response.setData(responseItem);
                response.setStatus(true);
                response.setMessage("Resultado de la lista de Items");
            }else{
                response.setStatus(false);
                response.setMessage("La Lista esta vacia");
            }
        }catch (Exception ex){
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return  response;
    }

    @Override
    public BaseResponse<List<ItemEntity>> obtenerItem(Integer id) {
        BaseResponse<List<ItemEntity>> response = new BaseResponse<>();
        try {
            List<ItemEntity> responseItem = itemMapper.obtenerItem(id);
            if (responseItem!=null){
                response.setData(responseItem);
                response.setStatus(true);
                response.setMessage("Se encontro el item");
            }else{
                response.setStatus(false);
                response.setMessage("La Lista esta vacia");
            }
        }catch (Exception ex){
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return  response;
    }

    @Override
    public BaseResponse<Integer> insertNewItemCloud(ItemEntity itemEntity) {
        BaseResponse<Integer> response = new BaseResponse<>();
        try {
           int responseItem = itemMapper.insertNewItemCloud(itemEntity);
            if (responseItem != 0){
                response.setData(responseItem);
                response.setStatus(true);
                response.setMessage("Se inserto el Producto correctamente");
            }else{
                response.setStatus(false);
                response.setMessage("No se logro Inserta el Producto");
            }
        }catch (Exception ex){
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return  response;
    }

    @Override
    public BaseResponse<Integer> putItemCloud(ItemEntity itemEntity) {
        BaseResponse<Integer> response = new BaseResponse<>();
        try {
            int responseItem = itemMapper.putItemCloud(itemEntity);
            if (responseItem != 0){
                response.setData(responseItem);
                response.setStatus(true);
                response.setMessage("Se modifico el Producto correctamente");
            }else{
                response.setStatus(false);
                response.setMessage("No se logro modificar el Producto");
            }
        }catch (Exception ex){
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return  response;
    }

    @Override
    public BaseResponse<Integer> deleteItemCloud(Integer id) {
        BaseResponse<Integer> response = new BaseResponse<>();
        try {
            int responseItem = itemMapper.deleteItemCloud(id);
            if (responseItem != 0){
                response.setData(responseItem);
                response.setStatus(true);
                response.setMessage("Se modifico el Producto correctamente");
            }else{
                response.setStatus(false);
                response.setMessage("No se logro modificar el Producto");
            }
        }catch (Exception ex){
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return  response;
    }

    public BaseResponse<String> uploadImageItem(MultipartFile file) {
        BaseResponse<String> response = new BaseResponse<>();

        if (!file.isEmpty()){
            String ext =  Objects.requireNonNull(file.getOriginalFilename()).substring(file.getOriginalFilename().lastIndexOf(".") + 1);
            ArrayList<String> typeFormatAceepted = new ArrayList<>();
            typeFormatAceepted.add("png");
            typeFormatAceepted.add("jpg");
            typeFormatAceepted.add("jpeg");

            if (typeFormatAceepted.contains(ext)){
                try {
                    byte[] bytes = file.getBytes();
                    Path path = Paths.get(UPLOAD_FOLDER+ file.getOriginalFilename());
                    Files.write(path, bytes);
                    setBaseResponse(response, true,"Se creo correctamente", "Se guardo la imágen correctamente");
                    System.out.println(response.getMessage());
                    System.out.println("Ya corrio la rutina de guardado)");
                }catch (IOException e){
                    e.printStackTrace();
                    setBaseResponse(response , false, "Ocurrio un problema al guardar la imágen", e.getMessage());
                }
            } else{
                setBaseResponse(response, false,"Ocurrio un problema al guardar la imágen", "El archivo no poesee un formato valido");
            }
        }else{
            setBaseResponse(response, false,"Ocurrio un problema al guardar la imágen", "Debe cargar una imagen");
        }

        return response;
    }

    public void setBaseResponse(BaseResponse<String> response, Boolean status, String message, String data){
        response.setStatus(status);
        response.setMessage(message);
        response.setData(data);
    }
}
