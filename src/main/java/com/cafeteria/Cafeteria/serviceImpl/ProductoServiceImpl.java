package com.cafeteria.Cafeteria.serviceImpl;

import com.cafeteria.Cafeteria.dao.CategoriaMapper;
import com.cafeteria.Cafeteria.dao.ProductoMapper;
import com.cafeteria.Cafeteria.domain.Categoria;
import com.cafeteria.Cafeteria.domain.Producto;
import com.cafeteria.Cafeteria.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("productoService")
public class ProductoServiceImpl implements ProductoService {
    @Autowired
    ProductoMapper productoMapper;

    //GET
    @Override
    public List<Producto> obtenerListProducto() {
        return productoMapper.obtenerListProducto();
    }
    ///GET
    @Override
    public List<Producto> selectxCategoria(String nomb_categoria) { return  productoMapper.selectoxCategoria(nomb_categoria); }
    //INSERT
    @Override
    public int insertarProducto(Producto producto){ return productoMapper.insertarProducto(producto); }
    //PUT
    @Override
    public int actualizarProducto(Producto producto) {
        return productoMapper.actualizarProducto(producto);
    }
    //DELETE
    @Override
    public int eliminarProducto(int referencia) { return productoMapper.eliminarProducto(referencia); }
}
