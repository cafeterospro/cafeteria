package com.cafeteria.Cafeteria.serviceImpl;

import com.cafeteria.Cafeteria.dao.DetalleOrdenMapper;
import com.cafeteria.Cafeteria.dao.OrdenMapper;
import com.cafeteria.Cafeteria.domain.*;
import com.cafeteria.Cafeteria.domain.request.UpdateStatusOrderRequest;
import com.cafeteria.Cafeteria.service.OrdenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service("OrdenService")

public class OrdenServiceImpl implements OrdenService {

    @Autowired
    OrdenMapper ordenMapper;

    @Autowired
    DetalleOrdenMapper detalleOrdenMapper;

    @Override
    public String obtenerConect() {
        return "hola mundo BEBE";
    }


    @Override
    public List<OrdenEntity> obtenerListOrdenes() {
        List<OrdenEntity> ordenEntityArrayList = new ArrayList<OrdenEntity>();
        ArrayList<DetalleOrden> i = new ArrayList<>();
        i.add(new DetalleOrden("HAMBURGUESA BASICA DE RES", 3, 5.0, 15.00, 1));
        ordenEntityArrayList.add(new OrdenEntity(1, "EN ESPERA", 15, i));
        ordenEntityArrayList.add(new OrdenEntity(2, "EN ESPERA", 20, i));
        return ordenEntityArrayList;
    }

    @Override
    public BaseResponse<ResponseOrden> insertarOrden(OrdenEntity ordenEntity) {
        BaseResponse<ResponseOrden> response = new BaseResponse<>();
        double totalOrden = 0;
        try {
            for (int i = 0; i < ordenEntity.getDetalleOrdenes().size(); i++) {
                totalOrden += ordenEntity.getDetalleOrdenes().get(i).getPreciototal();
            }

            ordenMapper.createNewOrden(ordenEntity.getCreated_by(), ordenEntity.getId_empresa(), totalOrden);

            for (int i = 0; i < ordenEntity.getDetalleOrdenes().size(); i++) {
                ordenMapper.insertItem(ordenMapper.lastInsert(),
                        ordenEntity.getDetalleOrdenes().get(i).getIditem(),
                        ordenEntity.getDetalleOrdenes().get(i).getCantidad(),
                        ordenEntity.getDetalleOrdenes().get(i).getPvp(),
                        ordenEntity.getDetalleOrdenes().get(i).getName(),
                        ordenEntity.getDetalleOrdenes().get(i).getPreciototal(),
                        ordenEntity.getId_empresa());
            }
            ResponseOrden responseOrden = new ResponseOrden();
            responseOrden.setIdOrden(ordenMapper.lastInsert());
            if (responseOrden.getIdOrden() != 0) {
                response.setData(responseOrden);
                response.setStatus(true);
                response.setMessage("Se creo la nueva OrdenEntity");
            } else {
                response.setStatus(false);
                response.setMessage("Error. No se creo la OrdenEntity");
            }
        } catch (Exception ex) {
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }

    public BaseResponse<List<OrdenEntity>> getListOdenesPaginate(int pagina, User user) {
        pagina = pagina * 10;
        BaseResponse<List<OrdenEntity>> response = new BaseResponse<>();
        try {
            List<OrdenAndDetalle> listItem = ordenMapper.getListOdenesPaginate(pagina, user.getId_empresa());
            List<OrdenEntity> responseItem = new ArrayList<>();
            if (listItem != null && !listItem.isEmpty()) {
                OrdenEntity orden = new OrdenEntity();
                for (int i = 0; i < listItem.size(); i++) {
                    OrdenAndDetalle ordenAndDetalle = listItem.get(i);
                    if (ordenAndDetalle.getNrofac() != 0 && ordenAndDetalle.getNrofac() == orden.getNrofac()) {
                        orden.getDetalleOrdenes().add(transform(ordenAndDetalle));
                    } else {
                        orden = new OrdenEntity();
                        orden.setNrofac(ordenAndDetalle.getNrofac());
                        orden.setFecha(ordenAndDetalle.getFecha());
                        orden.setEstado(ordenAndDetalle.getEstado());
                        orden.setMonto_total(ordenAndDetalle.getMonto_total());
                        orden.getDetalleOrdenes().add(transform(ordenAndDetalle));
                        orden.setId_status_order(ordenAndDetalle.getId_status_order());
                        responseItem.add(orden);
                    }
                }
            }
            response.setStatus(true);
            response.setMessage("Resultado de la lista de Ordenes");
            response.setData(responseItem);
        } catch (Exception ex) {
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }

    @Override
    public BaseResponse<Boolean> changeStatusOrder(UpdateStatusOrderRequest updateStatusOrderRequest) {
        BaseResponse<Boolean> response = new BaseResponse<>();
        try {
            Integer editedResponse = ordenMapper.putStatusOrder(updateStatusOrderRequest);
            if (editedResponse > 0) {
                response.setData(true);
                response.setStatus(true);
                response.setMessage("Se editó correctamente");
            } else {
                response.setStatus(false);
                response.setMessage("No se pudo editar la orden correctamente");
                response.setData(false);
            }
        } catch (Exception ex) {
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }

    private DetalleOrden transform(OrdenAndDetalle ordenAndDetalle) {
        DetalleOrden detalleOrden = new DetalleOrden();
        detalleOrden.setCantidad(ordenAndDetalle.getCantidad());
        detalleOrden.setIditem(ordenAndDetalle.getIditem());
        detalleOrden.setName(ordenAndDetalle.getName());
        detalleOrden.setNrofac(ordenAndDetalle.getNrofac());
        detalleOrden.setPreciototal(ordenAndDetalle.getPreciototal());
        detalleOrden.setPvp(ordenAndDetalle.getPvp());
        return detalleOrden;
    }
}
