package com.cafeteria.Cafeteria.serviceImpl;

import com.cafeteria.Cafeteria.dao.CategoriaMapper;
import com.cafeteria.Cafeteria.domain.BaseResponse;
import com.cafeteria.Cafeteria.domain.Categoria;
import com.cafeteria.Cafeteria.domain.User;
import com.cafeteria.Cafeteria.service.CategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("categoriaService")
public class CategoriaServiceImpl implements CategoriaService {

    @Autowired
    CategoriaMapper categoriaMapper;

    public BaseResponse<Categoria> getCategoryById(Integer id_categoria) {
        BaseResponse<Categoria> response = new BaseResponse<>();
        try {
            Categoria responseItem = categoriaMapper.getCategoryById(id_categoria);
            if (responseItem != null) {
                response.setData(responseItem);
                response.setStatus(true);
                response.setMessage("Se encotró la categoría");
            } else {
                response.setStatus(false);
                response.setMessage("No se encotró la categoría");
            }
        } catch (Exception ex) {
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }

    //GET
    @Override
    public BaseResponse<List<Categoria>> getListCategories(Integer id_empresa) {
        BaseResponse<List<Categoria>> response = new BaseResponse<>();
        try {
            List<Categoria> responseItem = categoriaMapper.getListCategories(id_empresa);
            if (responseItem != null) {
                response.setData(responseItem);
                response.setStatus(true);
                response.setMessage("Resultado de la lista de Categorías");
            } else {
                response.setStatus(false);
                response.setMessage("La Lista esta vacia");
            }
        } catch (Exception ex) {
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }

    @Override
    public BaseResponse<Integer> insertCategory(Categoria categoria) {
        BaseResponse<Integer> response = new BaseResponse<>();
        try {
            int responseCategory = categoriaMapper.insertCategory(categoria);
            if (responseCategory != 0) {
                response.setData(responseCategory);
                response.setStatus(true);
                response.setMessage("Se creo correctamente");
            } else {
                response.setStatus(false);
                response.setMessage("No se pudo crear correctamente");
            }
        } catch (Exception ex) {
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }

    @Override
    public BaseResponse<Integer> updateCategory(Categoria categoria) {
        BaseResponse<Integer> response = new BaseResponse<>();
        try {
            int responseCategory = categoriaMapper.updateCategory(categoria);
            if (responseCategory != 0) {
                response.setData(responseCategory);
                response.setStatus(true);
                response.setMessage("Se editó correctamente");
            } else {
                response.setStatus(false);
                response.setMessage("No se pudo editar correctamente");
            }
        } catch (Exception ex) {
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }

    @Override
    public BaseResponse<Integer> deleteCategory(int id_categoria) {
        BaseResponse<Integer> response = new BaseResponse<>();
        try {
            int responseCategory = categoriaMapper.deleteCategory(id_categoria);
            if (responseCategory != 0) {
                response.setData(responseCategory);
                response.setStatus(true);
                response.setMessage("Se elimino correctamente");
            } else {
                response.setStatus(false);
                response.setMessage("No se pudo eliminar correctamente");
            }
        } catch (Exception ex) {
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }
}
