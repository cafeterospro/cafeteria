package com.cafeteria.Cafeteria.serviceImpl;

import com.cafeteria.Cafeteria.dao.MarcaMapper;
import com.cafeteria.Cafeteria.domain.Marca;
import com.cafeteria.Cafeteria.service.MarcaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("marcaService")
public class MarcaServiceImpl implements MarcaService {
    @Autowired
    MarcaMapper marcaMapper;
    //GET
    @Override
    public List<Marca> obtenerListMarcas() { return marcaMapper.obtenerListMarcas(); }

    // INSERT
    @Override
    public int insertarMarca(Marca marca) {
        return marcaMapper.insertarMarca(marca);
    }

    @Override
    public int actualizarMarca(Marca marca) { return marcaMapper.actualizarMarca(marca);}

    @Override
    public int eliminarMarca (int id_marca) { return marcaMapper.eliminarMarca(id_marca);}
}