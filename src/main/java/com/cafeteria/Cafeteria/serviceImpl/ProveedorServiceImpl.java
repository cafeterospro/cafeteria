package com.cafeteria.Cafeteria.serviceImpl;

import com.cafeteria.Cafeteria.dao.ProveedorMapper;
import com.cafeteria.Cafeteria.domain.Proveedor;
import com.cafeteria.Cafeteria.service.ProveedorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("proveedorService")

public class ProveedorServiceImpl implements ProveedorService {
    @Autowired
    ProveedorMapper proveedorMapper;

    //GET
    @Override
    public List<Proveedor> obtenerListProveedor() {
        return proveedorMapper.obtenerListProveedor();
    }
    //INSERT
    @Override
    public int insertarProveedor(Proveedor proveedor){ return proveedorMapper.insertarProveedor(proveedor);}
    //PUT
    @Override
    public int actualizarProveedor(Proveedor proveedor) {
        return proveedorMapper.actualizarProveedor(proveedor);
    }
    //DELETE
    @Override
    public int eliminarProveedor(int ruc) {
        return proveedorMapper.eliminarProveedor(ruc);
    }
}
