package com.cafeteria.Cafeteria.serviceImpl;

import com.cafeteria.Cafeteria.dao.CompanyMapper;
import com.cafeteria.Cafeteria.domain.BaseResponse;
import com.cafeteria.Cafeteria.domain.CompanyEntity;
import com.cafeteria.Cafeteria.service.CompanyService;
import org.graalvm.compiler.replacements.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.logging.Level;
import java.util.logging.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;

@Service("companyService")
public class CompanyServiceImpl implements CompanyService {
    //Local proyect route
    //private String UPLOAD_FOLDER = ".//src//main//resources//files//";

    //private String UPLOAD_FOLDER = "//home//ubuntu//Proyects//HamburguesaGourmet//dev//assets//img//items//";
    //home/ubuntu/Proyects/HamburguesaGourmet/dev/assets
    //private String UPLOAD_FOLDER = "//home//united-commerce//data//img//";
    private String UPLOAD_FOLDER = "/opt/data/United-Commerce/";



    @Autowired
    CompanyMapper companyMapper;

    public BaseResponse<CompanyEntity> getCompanyById(Integer id_company) {
        BaseResponse<CompanyEntity> response = new BaseResponse<>();
        try {
            CompanyEntity responseCompany = companyMapper.getCompanyById(id_company);
            if (responseCompany != null) {
                response.setData(responseCompany);
                response.setStatus(true);
                response.setMessage("Se encotró la empresa");
            } else {
                response.setStatus(false);
                response.setMessage("No se encotró la empresa");
            }
        } catch (Exception ex) {
            response.setStatus(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }
}
