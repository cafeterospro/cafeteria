package com.cafeteria.Cafeteria.service;

import com.cafeteria.Cafeteria.domain.BaseResponse;
import com.cafeteria.Cafeteria.domain.Categoria;
import com.cafeteria.Cafeteria.domain.User;

import java.util.List;

public interface CategoriaService {
    BaseResponse<Categoria> getCategoryById(Integer id_categoria);
    BaseResponse<List<Categoria>> getListCategories(Integer id_empresa);
    BaseResponse<Integer> insertCategory(Categoria categoria);
    BaseResponse<Integer> updateCategory(Categoria categoria);
    BaseResponse<Integer> deleteCategory(int id_categoria);
}
