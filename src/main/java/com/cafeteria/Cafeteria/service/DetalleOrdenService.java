package com.cafeteria.Cafeteria.service;

import com.cafeteria.Cafeteria.domain.DetalleOrden;

import java.util.List;

public interface DetalleOrdenService{

    List<DetalleOrden> getListDetalleOrden();
    List<DetalleOrden> getDetalleOrden(int nrofac);
}
