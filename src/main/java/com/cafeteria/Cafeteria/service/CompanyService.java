package com.cafeteria.Cafeteria.service;

import com.cafeteria.Cafeteria.domain.BaseResponse;
import com.cafeteria.Cafeteria.domain.CompanyEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface CompanyService {

    BaseResponse<CompanyEntity> getCompanyById(Integer id_categoria);

}
