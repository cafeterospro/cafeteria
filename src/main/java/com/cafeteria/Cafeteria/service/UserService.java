package com.cafeteria.Cafeteria.service;

import com.cafeteria.Cafeteria.domain.BaseResponse;
import com.cafeteria.Cafeteria.domain.Categoria;
import com.cafeteria.Cafeteria.domain.User;

import java.util.List;

public interface UserService {

    //FUNCION DE LOGIN
    BaseResponse<User> loginUser(User user);

    BaseResponse<User> login(User user);

    BaseResponse<List<User>>searchUser(int nrodni);

    BaseResponse<List<User>> getListUsers(int id_empresa);

    BaseResponse<Integer> newUser(User user);

    BaseResponse<User> createUserAccount(User user);

    BaseResponse<Integer> updateUser(User user);

    BaseResponse<Integer> deleteUser(Integer nrodni);

}
