package com.cafeteria.Cafeteria.service;

import com.cafeteria.Cafeteria.domain.Marca;

import java.util.List;

public interface MarcaService {
    // SELECT
    List<Marca> obtenerListMarcas();
    //POST
    int insertarMarca(Marca marca);
    //PUT
    int actualizarMarca (Marca marca);
    //DELETE
    int eliminarMarca (int id_marca);
}
