package com.cafeteria.Cafeteria.service;

import com.cafeteria.Cafeteria.domain.BaseResponse;
import com.cafeteria.Cafeteria.domain.ItemEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ItemService {
    // SELECT
    BaseResponse<List<ItemEntity>> obtenerListItems(Integer id_empresa);

    // SELECT AL PRODUCTS FOR WEB
    BaseResponse<List<ItemEntity>> getListItemsWeb(Integer id_empresa);

    // SELECT BY ID
    BaseResponse<List<ItemEntity>> obtenerItem(Integer id);

    // POST
    BaseResponse<Integer> insertNewItemCloud(ItemEntity itemEntity);

    //PUT
    BaseResponse<Integer> putItemCloud(ItemEntity itemEntity);

    //DELETE
    BaseResponse<Integer> deleteItemCloud(Integer id);

    //SAVE IMAGE ITEM
    BaseResponse<String> uploadImageItem(MultipartFile file);

}
