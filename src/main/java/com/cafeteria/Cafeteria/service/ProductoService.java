package com.cafeteria.Cafeteria.service;

import com.cafeteria.Cafeteria.domain.Producto;

import java.util.List;

public interface ProductoService {
    // SELECT
    List<Producto> obtenerListProducto();
    // SELECT
    List<Producto> selectxCategoria(String nomb_categoria);
    //INSERT
    int insertarProducto(Producto producto);
    //PUT
    int actualizarProducto(Producto producto);
    //DELETE
    int eliminarProducto(int referencia);
}
