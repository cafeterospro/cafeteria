package com.cafeteria.Cafeteria.service;

import com.cafeteria.Cafeteria.domain.Proveedor;

import java.util.List;

public interface ProveedorService {
    // SELECT
    List<Proveedor> obtenerListProveedor();
    int insertarProveedor(Proveedor proveedor);
    //PUT
    int actualizarProveedor(Proveedor proveedor);
    //DELETE
    int eliminarProveedor(int ruc);
}
