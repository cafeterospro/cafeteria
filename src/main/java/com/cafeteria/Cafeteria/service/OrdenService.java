package com.cafeteria.Cafeteria.service;

import com.cafeteria.Cafeteria.domain.*;
import com.cafeteria.Cafeteria.domain.request.UpdateStatusOrderRequest;

import java.util.List;

public interface OrdenService {
    List<OrdenEntity> obtenerListOrdenes();

    String obtenerConect();

    BaseResponse<ResponseOrden> insertarOrden(OrdenEntity ordenEntity);

    BaseResponse<List<OrdenEntity>> getListOdenesPaginate(int pagina, User user);

    //PUT STATUS ORDER
    BaseResponse<Boolean> changeStatusOrder(UpdateStatusOrderRequest updateStatusOrderRequest);

}
