package com.cafeteria.Cafeteria.service.reportes;

import com.cafeteria.Cafeteria.domain.BaseResponse;
import com.cafeteria.Cafeteria.domain.reportes.ReporteVentasResponseEntity;

import java.util.List;

public interface ReporteVentasMesService {
    // GET
    BaseResponse<List<ReporteVentasResponseEntity>> obtenerListReporteVentasActual(int id_emoresa);
    // GET
    BaseResponse<List<ReporteVentasResponseEntity>> obtenerListReporteVentas(String fechaIni, String fechaFin);
    // GET
    BaseResponse<List<ReporteVentasResponseEntity>> obtenerListReporteVentasMesActual(int id_emoresa);
}
