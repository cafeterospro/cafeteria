package com.cafeteria.Cafeteria.domain;

public class Categoria {
    public int id_categoria;
    public String nomb_categoria;
    public String estado;
    public int id_empresa;

    public int getId_empresa() {
        return id_empresa;
    }

    public void setId_empresa(int id_empresa) {
        this.id_empresa = id_empresa;
    }

    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

    public String getNomb_categoria() {
        return nomb_categoria;
    }

    public void setNomb_categoria(String nomb_categoria) {
        this.nomb_categoria = nomb_categoria;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
