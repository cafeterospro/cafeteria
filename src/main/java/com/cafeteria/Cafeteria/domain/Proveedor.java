package com.cafeteria.Cafeteria.domain;

public class Proveedor {
    public int ruc;
    public String nombre_empresa;
    public String correo;
    public String direc_empresa;
    public String nombre_responsable;
    public String telef_responsable;

    public int getRuc() {
        return ruc;
    }

    public void setRuc(int ruc) {
        this.ruc = ruc;
    }

    public String getNombre_empresa() {
        return nombre_empresa;
    }

    public void setNombre_empresa(String nombre_empresa) {
        this.nombre_empresa = nombre_empresa;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDirec_empresa() {
        return direc_empresa;
    }

    public void setDirec_empresa(String direc_empresa) {
        this.direc_empresa = direc_empresa;
    }

    public String getNombre_responsable() {
        return nombre_responsable;
    }

    public void setNombre_responsable(String nombre_responsable) {
        this.nombre_responsable = nombre_responsable;
    }

    public String getTelef_responsable() {
        return telef_responsable;
    }

    public void setTelef_responsable(String telef_responsable) {
        this.telef_responsable = telef_responsable;
    }
}
