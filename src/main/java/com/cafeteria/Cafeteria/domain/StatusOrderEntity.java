package com.cafeteria.Cafeteria.domain;

public class StatusOrderEntity {
    private Integer id_order_status;
    private String name_order_status;

    public Integer getId_order_status() {
        return id_order_status;
    }

    public void setId_order_status(Integer id_order_status) {
        this.id_order_status = id_order_status;
    }

    public String getName_order_status() {
        return name_order_status;
    }

    public void setName_order_status(String name_order_status) {
        this.name_order_status = name_order_status;
    }
}
