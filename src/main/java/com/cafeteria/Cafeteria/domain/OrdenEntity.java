package com.cafeteria.Cafeteria.domain;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class OrdenEntity {
    private int nrofac;
    private String estado;
    private double monto_total;
    private Timestamp fecha;
    private int created_by;
    private int id_empresa;
    private int id_status_order;
    private List<DetalleOrden> detalleOrdenes = new ArrayList<>();

    public OrdenEntity() {
    }

    public OrdenEntity(int idOrden, String estado, double monto_total, List<DetalleOrden> detalleOrden){
        this.nrofac = idOrden;
        this.estado = estado;
        this.monto_total = monto_total;
        this.detalleOrdenes = detalleOrden;
    }

    public int getNrofac() {
        return nrofac;
    }

    public void setNrofac(int nrofac) {
        this.nrofac = nrofac;
    }

    public int getIdOrden() {
        return nrofac;
    }


    public void setIdOrden(int idOrden) {
        this.nrofac = idOrden;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public double getMonto_total() {
        return monto_total;
    }

    public void setMonto_total(double monto_total) {
        this.monto_total = monto_total;
    }

    public void setDetalleOrdenes(List<DetalleOrden> detalleOrdenes) {
        this.detalleOrdenes = detalleOrdenes;
    }

    public List<DetalleOrden> getDetalleOrdenes() {
        return detalleOrdenes;
    }

    public String getFecha() {
        String fechaFormateada = new SimpleDateFormat("dd/MM HH:mm").format(fecha);
        return fechaFormateada;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public int getCreated_by() {
        return created_by;
    }

    public void setCreated_by(int created_by) {
        this.created_by = created_by;
    }

    public int getId_empresa() {
        return id_empresa;
    }

    public void setId_empresa(int id_empresa) {
        this.id_empresa = id_empresa;
    }

    public int getId_status_order() {
        return id_status_order;
    }

    public void setId_status_order(int id_status_order) {
        this.id_status_order = id_status_order;
    }
}
