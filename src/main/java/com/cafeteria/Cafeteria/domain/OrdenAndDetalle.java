package com.cafeteria.Cafeteria.domain;

import java.sql.Timestamp;

public class OrdenAndDetalle {
    private int nrofac;
    private String estado;
    private double monto_total;
    private Timestamp fecha;
    private int created_by;
    private int iditem;
    private String name;
    private int cantidad;
    private double pvp;
    private double preciototal;
    private int id_status_order;

    public int getNrofac() {
        return nrofac;
    }

    public void setNrofac(int nrofac) {
        this.nrofac = nrofac;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public double getMonto_total() {
        return monto_total;
    }

    public void setMonto_total(double monto_total) {
        this.monto_total = monto_total;
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public int getCreated_by() {
        return created_by;
    }

    public void setCreated_by(int created_by) {
        this.created_by = created_by;
    }

    public int getIditem() {
        return iditem;
    }

    public void setIditem(int iditem) {
        this.iditem = iditem;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPvp() {
        return pvp;
    }

    public void setPvp(double pvp) {
        this.pvp = pvp;
    }

    public double getPreciototal() {
        return preciototal;
    }

    public void setPreciototal(double preciototal) {
        this.preciototal = preciototal;
    }

    public int getId_status_order() {
        return id_status_order;
    }

    public void setId_status_order(int id_status_order) {
        this.id_status_order = id_status_order;
    }
}
