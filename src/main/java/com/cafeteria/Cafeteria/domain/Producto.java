package com.cafeteria.Cafeteria.domain;

public class Producto {
    public int referencia;
    public String producto_nombre;
    public String nomb_marca;
    public String nomb_categoria;
    public String descripcion;
    public int existencia;
    public float costo;
    public float  pvp;
    public String estado;
    public String ubicacion;

    public int getReferencia() {
        return referencia;
    }

    public void setReferencia(int referencia) {
        this.referencia = referencia;
    }

    public String getProducto_nombre() { return producto_nombre;    }

    public void setProducto_nombre(String producto_nombre) { this.producto_nombre = producto_nombre; }

    public String getNomb_marca() {
        return nomb_marca;
    }

    public void setNomb_marca(String nomb_marca) {
        this.nomb_marca = nomb_marca;
    }

    public String getNomb_categoria() {
        return nomb_categoria;
    }

    public void setNomb_categoria(String nomb_categoria) {
        this.nomb_categoria = nomb_categoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getExistencia() {
        return existencia;
    }

    public void setExistencia(int existencia) {
        this.existencia = existencia;
    }

    public float getCosto() {
        return costo;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }

    public float getPvp() {
        return pvp;
    }

    public void setPvp(float pvp) {
        this.pvp = pvp;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
}
