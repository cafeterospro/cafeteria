package com.cafeteria.Cafeteria.domain.reportes;

public class ReporteVentasResponseEntity {

    private int iditem;
    private String name;
    //private String descripcion;
    private double costo;
    private double pvp;
    private int cantidad;
    private double costo_total;
    private double totalxproducto;

    public int getIditem() {
        return iditem;
    }

    public void setIditem(int iditem) {
        this.iditem = iditem;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPvp() {
        return pvp;
    }

    public void setPvp(double pvp) {
        this.pvp = pvp;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getTotalxproducto() {
        return totalxproducto;
    }

    public void setTotalxproducto(double totalxproducto) {
        this.totalxproducto = totalxproducto;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public double getCosto_total() {
        return costo_total;
    }

    public void setCosto_total(double costo_total) {
        this.costo_total = costo_total;
    }
}
