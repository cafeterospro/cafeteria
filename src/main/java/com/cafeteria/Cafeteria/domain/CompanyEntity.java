package com.cafeteria.Cafeteria.domain;

public class CompanyEntity {
    private int id_empresa;
    private String codigo;
    private String nomb_empresa;
    private String correo;
    private String telefono;
    private String localidad;
    private String direccion;

    public int getId_empresa() {
        return id_empresa;
    }

    public void setId_empresa(int id_empresa) {
        this.id_empresa = id_empresa;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNomb_empresa() {
        return nomb_empresa;
    }

    public void setNomb_empresa(String nomb_empresa) {
        this.nomb_empresa = nomb_empresa;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

}
