package com.cafeteria.Cafeteria.domain;

public class DetalleOrden {
    private int nrofac;
    private int iditem;
    private String name;
    private int cantidad;
    private double pvp;
    private double preciototal;
    private int id_empresa;


    public int getNrofac() {
        return nrofac;
    }

    public void setNrofac(int nrofac) {
        this.nrofac = nrofac;
    }

    public int getIditem() {
        return iditem;
    }

    public void setIditem(int iditem) {
        this.iditem = iditem;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPvp() {
        return pvp;
    }

    public void setPvp(double pvp) {
        this.pvp = pvp;
    }

    public double getPreciototal() {
        return preciototal;
    }

    public void setPreciototal(double preciototal) {
        this.preciototal = preciototal;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public DetalleOrden(){

    }

    public DetalleOrden(String nombre, int cantidad, double pvp, double preciototal, int id_empresa) {
        this.name = nombre;
        this.cantidad = cantidad;
        this.pvp = pvp;
        this.preciototal = preciototal;
        this.id_empresa = id_empresa;
    }


    public int getId_empresa() {
        return id_empresa;
    }

    public void setId_empresa(int id_empresa) {
        this.id_empresa = id_empresa;
    }
}
