package com.cafeteria.Cafeteria.domain;

public class Marca {
        private Integer id_marca;
        private String nomb_marca;
        private String estado;

    public Integer getId_marca() {
        return id_marca;
    }

    public void setId_marca(Integer id_marca) {
        this.id_marca = id_marca;
    }

    public String getNomb_marca() {
        return nomb_marca;
    }

    public void setNomb_marca(String nomb_marca) {
        this.nomb_marca = nomb_marca;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
