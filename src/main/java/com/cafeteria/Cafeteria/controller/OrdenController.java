package com.cafeteria.Cafeteria.controller;

import com.cafeteria.Cafeteria.domain.*;
import com.cafeteria.Cafeteria.domain.request.UpdateStatusOrderRequest;
import com.cafeteria.Cafeteria.service.OrdenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController

public class OrdenController {

    @Autowired
    private OrdenService ordenService;

    // GET
    @RequestMapping(value = "/ordenes", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public List<OrdenEntity> obtenerOrdenes() {
        return ordenService.obtenerListOrdenes();
    }

    // GET PAGINACIÓN
    @RequestMapping(value = "/getListOrdenesPaginate/{pagina}", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<List<OrdenEntity>> getListOrdenesPaginate(@PathVariable int pagina, @RequestBody() User user) {
        return ordenService.getListOdenesPaginate(pagina, user);
    }

    // GET
    @RequestMapping(value = "/pruebaConect", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public String pruebaConect() {
        return ordenService.obtenerConect();
    }

    //POST
    @RequestMapping(value = "/ordenPost" , method = RequestMethod.POST)
    public BaseResponse<ResponseOrden> insertarOrden(@RequestBody() OrdenEntity ordenEntity){
        return ordenService.insertarOrden(ordenEntity);
    }

    //PUT STATUS ORDEN
    @RequestMapping(value = "/changeStatusOrder" , method = RequestMethod.PUT)
    public BaseResponse<Boolean> changeStatusOrder(@RequestBody UpdateStatusOrderRequest updateStatusOrderRequest){
            return ordenService.changeStatusOrder(updateStatusOrderRequest);
    }
}
