package com.cafeteria.Cafeteria.controller;

import com.cafeteria.Cafeteria.domain.BaseResponse;
import com.cafeteria.Cafeteria.domain.Categoria;
import com.cafeteria.Cafeteria.domain.User;
import com.cafeteria.Cafeteria.service.CategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
public class CategoriaController {
    @Autowired
    private CategoriaService categoriaService;

    // GET CATEGORY BY ID
    @RequestMapping(value = "/getCategoryById/{id_categoria}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<Categoria>getCategoryById(@PathVariable int id_categoria) {
        return categoriaService.getCategoryById(id_categoria);
    }
    // GET
    @RequestMapping(value = "/getListCategories", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<List<Categoria>>getListCategories(@RequestBody() User user) {
        return categoriaService.getListCategories(user.getId_empresa());
    }

    // POST
    @RequestMapping(value = "/newCategory", method = RequestMethod.POST)
    public BaseResponse<Integer> insertCategory(@RequestBody() Categoria categoria) {
        return categoriaService.insertCategory(categoria);
    }

    // PUT
    @RequestMapping(value = "/updateCategory", method = RequestMethod.PUT)
    public BaseResponse<Integer> updateCategory(@RequestBody() Categoria categoria) {
        return categoriaService.updateCategory(categoria);
    }

    //DELETE
    @RequestMapping(value = "/deleteCategory/{id_categoria}", method = RequestMethod.PUT)
    public BaseResponse<Integer> eliminarCategoria(@PathVariable int id_categoria) {
        return categoriaService.deleteCategory(id_categoria);
    }
}
