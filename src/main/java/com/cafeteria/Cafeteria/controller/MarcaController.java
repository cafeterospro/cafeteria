package com.cafeteria.Cafeteria.controller;

import com.cafeteria.Cafeteria.domain.Marca;
import com.cafeteria.Cafeteria.service.MarcaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin // esto le otrorga permisos!
@RestController // esto es para que lo pudas consultar con una ruta!
public class MarcaController {
    @Autowired // esto no me recuerdo!
    private MarcaService marcaService;

    // GET
    @RequestMapping(value = "/marcas", method = RequestMethod.GET) // esto le idica a ruta en el paht
    @ResponseStatus(value = HttpStatus.OK) // esto le avisa que es un http
    public List<Marca> obtenerMarcas() {
        //System.out.println("Obtener maracas");
        return  marcaService.obtenerListMarcas();
    }

    // POST
    @RequestMapping(value = "/marcapost" , method = RequestMethod.POST)
    public int insertarMarca(@RequestBody() Marca marca){
        return marcaService.insertarMarca(marca);
    } //@RequestBody() esto indica que los datos vendran en el cuerpo del mensaje ojo! porque para entenderlo sufri

    // PUT
    @RequestMapping(value = "/marcaput" , method = RequestMethod.PUT)
    public int actualizarMarca(@RequestBody() Marca marca){
        return marcaService.actualizarMarca(marca);
    }

    //DELETE
    @RequestMapping(value = "/marcadelete/{id_marca}", method = RequestMethod.DELETE)
    public int eliminarMarca(@PathVariable int id_marca) { return marcaService.eliminarMarca(id_marca); //Esto indica que el dato vendra en el path
    }
}
