package com.cafeteria.Cafeteria.controller;

import com.cafeteria.Cafeteria.domain.Producto;
import com.cafeteria.Cafeteria.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
public class ProductoController {
        @Autowired
        private ProductoService productoService;

        // GET
        @RequestMapping(value = "/productos", method = RequestMethod.GET)
        @ResponseStatus(value = HttpStatus.OK)
        public List<Producto> obtenerProducto() {
            return productoService.obtenerListProducto();
        }
        //GET BY NOMBRE
        @RequestMapping(value = "/selectProductosxCategoria/{nomb_categoria}", method = RequestMethod.GET)
        public List<Producto> selectxCategoria(@PathVariable String nomb_categoria) {
                return productoService.selectxCategoria(nomb_categoria);
        }
        // INSERT
        @RequestMapping(value = "/productopost", method = RequestMethod.POST)
        @ResponseStatus(value = HttpStatus.OK)
        public int agregarProducto(@RequestBody() Producto producto){
            return productoService.insertarProducto(producto);
        }
        //PUT
        @RequestMapping(value =  "/productoput", method =  RequestMethod.PUT)
        @ResponseStatus(value = HttpStatus.OK)
        public  int actualizarProducto(@RequestBody() Producto producto){   return productoService.actualizarProducto(producto);}
        //DELETE
        @RequestMapping(value = "/productodelete", method = RequestMethod.DELETE)
        @ResponseStatus(value = HttpStatus.OK)
        public int eliminarProducto(@PathVariable int referencia) {return productoService.eliminarProducto(referencia);}
        //prueba pasa subir a git luego de la autenticacion
}
