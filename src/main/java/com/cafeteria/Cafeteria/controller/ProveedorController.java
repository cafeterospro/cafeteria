package com.cafeteria.Cafeteria.controller;

import com.cafeteria.Cafeteria.domain.Proveedor;
import com.cafeteria.Cafeteria.service.ProveedorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController

public class ProveedorController {
    @Autowired
    private ProveedorService proveedorService;
    //GET
    @RequestMapping(value = "/proveedor", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public List<Proveedor> obtenerProducto() {
        return proveedorService.obtenerListProveedor();
    }
    // INSERT
    @RequestMapping(value = "/proveedorpost", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public int agregarProducto(@RequestBody() Proveedor proveedor){
        return proveedorService.insertarProveedor(proveedor);}
    //PUT
    @RequestMapping(value =  "/proveedorput", method =  RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public  int actualizarProveedor(@RequestBody() Proveedor proveedor){   return proveedorService.actualizarProveedor(proveedor);}
    //DELETE
    @RequestMapping(value = "/proveedordelete", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public int eliminarProveedor(@PathVariable int ruc) {return proveedorService.eliminarProveedor(ruc);}
}
