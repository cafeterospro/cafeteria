package com.cafeteria.Cafeteria.controller;

import com.cafeteria.Cafeteria.domain.BaseResponse;
import com.cafeteria.Cafeteria.domain.ItemEntity;
import com.cafeteria.Cafeteria.domain.User;
import com.cafeteria.Cafeteria.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin // esto le otrorga permisos!
@RestController // esto es para que lo pudas consultar con una ruta!

public class UserController {
    @Autowired
    private UserService userService;

    //LOGIN
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public BaseResponse<User> loginUser(@RequestBody User user) {
        return userService.loginUser(user);
    }

    //LOGIN
    @RequestMapping(value = "/v1/login", method = RequestMethod.POST)
    public BaseResponse<User> login(@RequestBody User user) {
        return userService.login(user);
    }

    //POST
    @RequestMapping(value = "/getListUsers", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<List<User>> getListUsers(@RequestBody() User user) {
        return  userService.getListUsers(user.getId_empresa());}

    @RequestMapping(value = "/newUser", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<Integer> newUser(@RequestBody() User user) {
        return  userService.newUser(user);
    }

    @RequestMapping(value = "v1/user/create", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<User> createUserAccount(@RequestBody() User user) {
        return  userService.createUserAccount(user);
    }

    @RequestMapping(value = "/updateUser", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<Integer> updateUser(@RequestBody() User user) {
        return  userService.updateUser(user);
    }

    @RequestMapping(value =  "/deleteUser/{nrodni}", method =  RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<Integer> deleteUser(@PathVariable int nrodni){
        return userService.deleteUser(nrodni);
    }

    //SEARCH USER BY DNI
    @RequestMapping(value =  "/searchUser/{nrodni}", method =  RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<List<User>> searchUser(@PathVariable int nrodni){
        return userService.searchUser(nrodni);
    }

}
