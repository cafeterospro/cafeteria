package com.cafeteria.Cafeteria.controller;

import com.cafeteria.Cafeteria.domain.DetalleOrden;
import com.cafeteria.Cafeteria.service.DetalleOrdenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController

public class DetalleOrdenController {

    @Autowired
    private DetalleOrdenService detalleOrdenService;

    @RequestMapping(value = "/getListDetalleOrden", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public List<DetalleOrden> obtenerAllDetalleOrden() {
        return  detalleOrdenService.getListDetalleOrden();
    }


    @RequestMapping(value = "/getDetalleOrden/{nrofac}", method = RequestMethod.GET)
    public List<DetalleOrden> obtenerDetalleOrden(@PathVariable int nrofac) {
        return  detalleOrdenService.getDetalleOrden(nrofac);
    }
}
