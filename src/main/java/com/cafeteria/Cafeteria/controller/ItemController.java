package com.cafeteria.Cafeteria.controller;


import com.cafeteria.Cafeteria.domain.BaseResponse;
import com.cafeteria.Cafeteria.domain.ItemEntity;
import com.cafeteria.Cafeteria.domain.User;
import com.cafeteria.Cafeteria.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@CrossOrigin
@RestController
public class ItemController {
    @Autowired
    private ItemService itemService;

    //GET
    @RequestMapping(value = "/getItems", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<List<ItemEntity>> obtenerItems(@RequestBody() User user) {
        return  itemService.obtenerListItems(user.getId_empresa());
    }

    //GET PRODUCT LIST WEB
    @RequestMapping(value = "/getitemsweb", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<List<ItemEntity>> obtenerListItemsWeb(@RequestBody() User user) {
        return  itemService.getListItemsWeb(user.getId_empresa());
    }

    //GET by ID
    @RequestMapping(value = "/getitem/{id}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<List<ItemEntity>> obtenerItem(@PathVariable int id) {
        return  itemService.obtenerItem(id);
    }

    // POST
    @RequestMapping(value = "/insertnewitem", method = RequestMethod.POST)
    public BaseResponse<Integer> insertNewItemCloud(@RequestBody() ItemEntity itemEntity) {
        return itemService.insertNewItemCloud(itemEntity);
    }

    @RequestMapping(value =  "/putitem", method =  RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<Integer> putItemCloud(@RequestBody() ItemEntity itemEntity){
        return itemService.putItemCloud(itemEntity);
    }

    @RequestMapping(value =  "/deleteitem/{id}", method =  RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<Integer> deleteItemCloud(@PathVariable int id){
        return itemService.deleteItemCloud(id);
    }


    // GET COMPANY BY ID
    @RequestMapping(value = "/uploadimagecompany", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<String> uploadImageCompany(@RequestParam("file") MultipartFile file) {
        return itemService.uploadImageItem(file);
    }
}
