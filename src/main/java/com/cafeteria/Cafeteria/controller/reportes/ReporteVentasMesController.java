package com.cafeteria.Cafeteria.controller.reportes;

import com.cafeteria.Cafeteria.domain.BaseResponse;
import com.cafeteria.Cafeteria.domain.User;
import com.cafeteria.Cafeteria.domain.reportes.ReporteVentasResponseEntity;
import com.cafeteria.Cafeteria.service.reportes.ReporteVentasMesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class ReporteVentasMesController {
        @Autowired
        private ReporteVentasMesService reporteVentasMesService;

        // GET
        @RequestMapping(value = "/getReporteVentasDiaActual", method = RequestMethod.POST)
        @ResponseStatus(value = HttpStatus.OK)
        public BaseResponse<List<ReporteVentasResponseEntity>> obtenerReporteVentasMesActual(@RequestBody() User user) {
            return reporteVentasMesService.obtenerListReporteVentasActual(user.getId_empresa());
        }

        @RequestMapping(value = "/getReporteVentasMes/{fechaIni}/{fechaFin}", method = RequestMethod.GET)
        public BaseResponse<List<ReporteVentasResponseEntity>> obtenerReporteVentas(@PathVariable String fechaIni, @PathVariable String fechaFin) {
                return  reporteVentasMesService.obtenerListReporteVentas(fechaIni, fechaFin);
        }

        @RequestMapping(value = "/getReporteVentasMesActual", method = RequestMethod.POST)
        public BaseResponse<List<ReporteVentasResponseEntity>> obtenerListReporteVentasMesActual(@RequestBody() User user) {
                return  reporteVentasMesService.obtenerListReporteVentasMesActual(user.getId_empresa());
        }


}
