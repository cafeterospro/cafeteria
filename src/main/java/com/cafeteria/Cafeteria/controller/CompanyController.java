package com.cafeteria.Cafeteria.controller;

import com.cafeteria.Cafeteria.domain.BaseResponse;
import com.cafeteria.Cafeteria.domain.CompanyEntity;
import com.cafeteria.Cafeteria.service.CompanyService;
import org.graalvm.compiler.replacements.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin
@RestController
public class CompanyController {
    @Autowired
    private CompanyService companyService;

    // GET COMPANY BY ID
    @RequestMapping(value = "/getCompanyById/{id_company}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public BaseResponse<CompanyEntity> getCompanyById(@PathVariable int id_company) {
        return companyService.getCompanyById(id_company);
    }

}
