package com.cafeteria.Cafeteria.dao;

import com.cafeteria.Cafeteria.domain.ItemEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface ItemMapper {

    //SELECT
    @Select("select iditem, nombre, nombcategoria, descripcion, costo, estado, precio_costo, id_categoria, id_empresa, image_path from item where estado != 'inactivo' and id_empresa = #{id_empresa}")
    List<ItemEntity> obtenerListItems(@Param("id_empresa") int id_empresa);

    //SELECT ALL PRODUCTS WEB
    @Select("select iditem, nombre, nombcategoria, descripcion, costo, estado, precio_costo, id_categoria, id_empresa , image_path from item where id_empresa = #{id_empresa}")
    List<ItemEntity> getListItemsWeb(@Param("id_empresa") int id_empresa);

    //SELECT BY ID
    @Select("select iditem, nombre, nombcategoria, descripcion, costo, estado, precio_costo, id_categoria, id_empresa, image_path from item where iditem = #{id}")
    List<ItemEntity> obtenerItem(Integer id);

    //INSERT
    @Insert("INSERT INTO item (nombre, nombcategoria, descripcion, costo, precio_costo, estado, id_categoria, id_empresa, image_path ) VALUES (#{nombre},#{nombcategoria},#{descripcion},#{costo}, #{precio_costo}, #{estado}, #{id_categoria}, #{id_empresa}, #{image_path} )")
    int insertNewItemCloud(ItemEntity itemEntity);

    //PUT
    @Update("update item set nombre = #{nombre}, nombcategoria = #{nombcategoria}, descripcion = #{descripcion}, costo = #{costo}, precio_costo = #{precio_costo}, id_categoria = #{id_categoria}, estado = #{estado}, image_path = #{image_path}" +
            " where iditem = #{iditem}")
    int putItemCloud(ItemEntity itemEntity);

    @Update("update item set estado = 'inactivo'" +
            " where iditem = #{id}")
    int deleteItemCloud(Integer id);
}