package com.cafeteria.Cafeteria.dao;

import com.cafeteria.Cafeteria.domain.Categoria;
import org.apache.ibatis.annotations.*;

import java.util.List;


public interface CategoriaMapper {


    // SELECT CATEGORY BY ID
    @Select("select id_categoria, nomb_categoria, estado, id_empresa from categoria where id_categoria = #{id_categoria}")
    Categoria getCategoryById(@Param("id_categoria") int id_categoria);

    // SELECT ALL CATEGORIES
    @Select("select id_categoria, nomb_categoria, estado, id_empresa from categoria where id_empresa = #{id_empresa}")
    List<Categoria> getListCategories(@Param("id_empresa") int id_empresa);

    //INSERT
    @Insert("insert into categoria (nomb_categoria, id_empresa) values(#{nomb_categoria}, #{id_empresa})")
    int insertCategory(Categoria categoria);

    //UPDATE
    @Update("update categoria set nomb_categoria = #{nomb_categoria}, estado = #{estado} where id_categoria = #{id_categoria}")
    int updateCategory(Categoria categoria);

    // DELETE
    @Update("update categoria set estado= 'inactivo' where id_categoria = #{id_categoria}")
    int  deleteCategory(@Param("id_categoria") int id_categoria);
}
