package com.cafeteria.Cafeteria.dao;

import com.cafeteria.Cafeteria.domain.CompanyEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface CompanyMapper {

    // SELECT CATEGORY BY ID
    @Select("select id_empresa, codigo, nomb_empresa, correo, telefono, localidad, direccion from empresa where id_empresa = #{id_company}")
    CompanyEntity getCompanyById(@Param("id_company") int id_company);
}
