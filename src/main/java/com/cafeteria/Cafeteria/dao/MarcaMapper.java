package com.cafeteria.Cafeteria.dao;

import com.cafeteria.Cafeteria.domain.Marca;
import org.apache.ibatis.annotations.*;

import java.util.ArrayList;

public interface MarcaMapper{
        // SELECT ALL MARCAS

        @Select("select id_marca, nomb_marca, estado from marca")
        ArrayList<Marca> obtenerListMarcas();

        //INSERT
        @Insert("insert into marca (nomb_marca) values(#{nomb_marca})")
        int insertarMarca(Marca marca);

        //UPDATE
        @Update("update marca set nomb_marca = #{nomb_marca}, estado = #{estado} where id_marca = #{id_marca}")
        int actualizarMarca(Marca marca);

        // DELETE

        @Delete("delete from marca where id_marca = #{id_marca}")
        int  eliminarMarca(@Param("id_marca") int id_marca);
}


