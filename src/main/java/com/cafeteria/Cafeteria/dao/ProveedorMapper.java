package com.cafeteria.Cafeteria.dao;

import com.cafeteria.Cafeteria.domain.Proveedor;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface ProveedorMapper {
    //SELECT
    @Select("select * from proveedor")
    List<Proveedor> obtenerListProveedor();
    //INSERT
    @Insert("insert into proveedor (ruc,nombre_empresa,correo,direc_empresa,nombre_responsable,telef_responsable) values(#{ruc},#{nombre_empresa},#{correo},#{direc_empresa},#{nombre_responsable},#{telef_responsable})")
    int insertarProveedor(Proveedor proveedor);
    //PUT
    @Update("update proveedor set ruc = #{ruc}, nombre_empresa= #{nombre_empresa}  where ruc = #{ruc}")
    int actualizarProveedor(Proveedor proveedor);
    //DELETE
    @Delete("delete proveedor where ruc = #{ruc}")
    int eliminarProveedor(int ruc);
}
