package com.cafeteria.Cafeteria.dao;

import com.cafeteria.Cafeteria.domain.Producto;
import org.apache.ibatis.annotations.*;

import java.util.List;

import static org.postgresql.jdbc2.EscapedFunctions.INSERT;

public interface ProductoMapper {
    //SELECT
    @Select("select * from producto")
    List<Producto> obtenerListProducto();
    // SELECT PRODUCTOS WHIT THE SAME CATEGORIA
    @Select("select * from producto where nomb_categoria = #{nomb_categoria}")
    List<Producto> selectoxCategoria(String nomb_categoria);
    //INSERT
    @Insert("insert into producto (referencia, producto_nombre, nomb_marca,nomb_categoria, descripcion, existencia, costo, pvp, estado, ubicacion) values(#{referencia},#{producto_nombre},#{nomb_marca},#{nomb_categoria},#{descripcion},3,10,13,'activo', 'ubicacion')")
    int insertarProducto(Producto producto);
    //PUT
    @Update("update producto set referencia = #{referencia}, nomb_marca = #{nomb_marca}, nomb_categoria = #{nomb_categoria } where referencia = #{referencia}")
    int actualizarProducto(Producto producto);
    //DELETE
    @Delete("delete producto where referencia = #{referencia}")
    int eliminarProducto(int referencia);
}
