package com.cafeteria.Cafeteria.dao.reportes;

import com.cafeteria.Cafeteria.domain.reportes.ReporteVentasResponseEntity;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface ReporteVentasMesMapper {
    @Select("select iditem, name , (select precio_costo from item where iditem = detalle_factventa.idItem) AS costo, pvp, SUM(cantidad) as cantidad, ((select precio_costo from item where iditem = detalle_factventa.idItem) * (SUM(cantidad))) AS costo_total, SUM(preciototal) as totalxproducto \n" +
            "from detalle_factventa where nrofac in \n" +
            "(select nrofac from fact_venta where fecha between current_date and current_timestamp) and id_empresa = ${id_empresa}group by iditem, name, pvp;")
    List<ReporteVentasResponseEntity> obtenerListReporteVentasActual(@Param("id_empresa") int id_empresa);


    @Select("select iditem, name , (select precio_costo from item where iditem = detalle_factventa.idItem) AS costo, pvp, SUM(cantidad) as cantidad, ((select precio_costo from item where iditem = detalle_factventa.idItem) * (SUM(cantidad))) AS costo_total, SUM(preciototal) as totalxproducto \n" +
            "from detalle_factventa where nrofac in \n" +
            "(select nrofac from fact_venta where fecha between '${ini}' and '${fin}')group by iditem, name, pvp;")
    List<ReporteVentasResponseEntity> obtenerListReporteVentas(@Param("ini") String fechaini, @Param("fin") String fechafin);

    @Select("select iditem, name , (select precio_costo from item where iditem = detalle_factventa.idItem) AS costo, pvp, SUM(cantidad) as cantidad, ((select precio_costo from item where iditem = detalle_factventa.idItem) * (SUM(cantidad))) AS costo_total, SUM(preciototal) as totalxproducto\n" +
            "from detalle_factventa where nrofac in\n" +
            "(select nrofac from fact_venta where fecha between (select date_trunc('month', current_date))\n" +
            "and current_timestamp) and id_empresa = ${id_empresa} group by iditem, name, pvp;")
    List<ReporteVentasResponseEntity> obtenerListReporteVentasMesActual(@Param("id_empresa") int id_empresa);
}
