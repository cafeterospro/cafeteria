package com.cafeteria.Cafeteria.dao;

import com.cafeteria.Cafeteria.domain.User;
import org.apache.ibatis.annotations.*;


import java.util.ArrayList;

public interface UserMapper {
    // SELECT ALL MARCAS


    @Select("select nrodni, clave, nivel, nombre, apellido, telefono, direccion, estado from usuario where nrodni = #{nrodni}")
    ArrayList<User> searchUser(@Param("nrodni")int nrodni);

    @Select("select nrodni, nivel, nombre, apellido, telefono, direccion, id_empresa, estado from usuario where  nrodni = #{nrodni} and clave = #{clave}")
    User loginUser(User user);

    @Select("select nrodni, nivel, nombre, apellido, telefono, direccion, id_empresa, estado, email from usuario where  email = #{email} and clave = #{clave}")
    User login(User user);

    @Insert("insert into usuario (nrodni, clave, nivel, nombre, apellido, telefono, direccion, id_empresa, estado, email) values(#{nrodni} , #{clave}, #{nivel}, #{nombre}, #{apellido}, #{telefono}, #{direccion}, #{id_empresa}, #{estado}, #{email} )")
    int newUser(User user);

    @Insert("insert into usuario (nrodni, clave, nivel, nombre, apellido, telefono, direccion, id_empresa, estado, email) values(#{nrodni} , #{clave}, #{nivel}, #{nombre}, #{apellido}, #{telefono}, #{direccion}, #{id_empresa}, #{estado}, #{email} )")
    int createUserAccount(User user);

    @Select("select nrodni, clave, nivel, nombre, apellido, telefono, direccion, id_empresa, estado from usuario where  id_empresa = #{id_empresa}")
    ArrayList<User> getListUsers(@Param("id_empresa")int id_empresa);

    @Update("UPDATE usuario SET clave = #{clave}, nombre = #{nombre}, apellido = #{apellido}, telefono = #{telefono}, direccion = #{direccion}, estado = #{estado} WHERE nrodni = #{nrodni}")
    int updateUser(User user);

    @Update("update usuario set estado = 'inactivo' where nrodni = #{nrodni}")
    int deleteUser(Integer nrodni);
}
