package com.cafeteria.Cafeteria.dao;

import com.cafeteria.Cafeteria.domain.DetalleOrden;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface DetalleOrdenMapper {

    @Select("select nrofac, cantidad, pvp, name, iditem, preciototal from detalle_factventa")
    List<DetalleOrden> getListDetalleOrden();

    @Select("select nrofac, cantidad, pvp, name, iditem, preciototal from detalle_factventa where nrofac = #{nrofac}")
    List<DetalleOrden> getDetalleOrden(@Param("nrofac")int nrofac);
}
