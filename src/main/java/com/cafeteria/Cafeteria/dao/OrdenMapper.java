package com.cafeteria.Cafeteria.dao;


import com.cafeteria.Cafeteria.domain.OrdenAndDetalle;
import com.cafeteria.Cafeteria.domain.request.UpdateStatusOrderRequest;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface OrdenMapper {

    @Insert("insert into fact_venta (monto_total, created_by, id_empresa) values (#{totalOrden}, #{created_by}, #{id_empresa})")
    int createNewOrden(@Param("created_by") int created_by, @Param("id_empresa") int id_empresa, @Param("totalOrden") double totalOrden);

    @Select("SELECT * FROM fact_venta ORDER BY nrofac DESC LIMIT 1")
    int lastInsert();

    @Insert("insert into detalle_factventa (nrofac,iditem, cantidad, pvp, name, preciototal, id_empresa) values (#{odenActual},#{idItem},#{cantidad},#{pvp}, #{name}, #{preciototal}, #{id_empresa}) ")
    int insertItem(@Param("odenActual") int odenActual, @Param("idItem") int idItem, @Param("cantidad") int cantidad, @Param("pvp") double pvp, @Param("name") String name,  @Param("preciototal")double precioTotal, @Param("id_empresa")int id_empresa);

    @Select(" SELECT F.nrofac, F.monto_total, F.fecha, F.estado, F.created_by,  F.id_status_order, D.cantidad, D.pvp, D.name, D.iditem, D.preciototal\n" +
            "    FROM(SELECT * FROM fact_venta WHERE id_empresa = #{id_empresa} ORDER BY nrofac DESC LIMIT 10 OFFSET #{pagina}) F\n" +
            "    INNER JOIN detalle_factventa D ON F.nrofac = D.nrofac ORDER BY D.nrofac DESC")
    List<OrdenAndDetalle> getListOdenesPaginate(@Param("pagina") int pagina, @Param("id_empresa") int id_empresa);

    @Update("update fact_venta set id_status_order = #{newStatus} where id_empresa = #{id_empresa} AND nrofac = #{idOrder}")
    int putStatusOrder(UpdateStatusOrderRequest updateStatusOrderRequest);

}